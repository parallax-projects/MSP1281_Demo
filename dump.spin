CON


VAR
    long cog_id
    long lock_id
    long dumpstack[1024]

OBJ
    term            : "Parallax_Serial_Terminal"
'    text        :        "std_text_routines"
'    commandparser   : "commandparser"
'    str             : "string"
'    num             : "string.integer"


PUB start(address) : retval | kcol
        ' initialization
     term.Start(115200)
     'text.start_default(115200)
'    term.hex(address, 5)
'    term.nl
    cog_id := cognew(dumpspin(address), @dumpstack)
'    term.hex(cog_id,8)
'    term.CRLF
'    text.hex(cog_id,4)

    kcol := locknew
    ifnot(kcol == -1)
        lock_id := kcol
        'cog_id := cognew(dumpspin(address), @dumpstack)
        ifnot(cog_id == -1)
            retval := lock_id
        else
            lockret(lock_id)    ' cannot get a cog so return the lock

    return

PRI dumpspin(address) | index, adr, data

'    term.start_default(115200)
    repeat
        waitcnt(100000000 + cnt)
        ifnot(lock_id == -1)
            repeat until not lockset(lock_id)

        'term.Str(@CLR)
        term.Home
        adr := address
        repeat index from 0 to 511
            if((index // 16) == 0)
                term.NewLine
                term.Hex(adr, 4)
                term.Char($20)
                term.Hex(index, 3)
                term.Char($20) ' give me some space
                data := long[adr]
                'data := libi(data)
                adr += 4
                term.hex(data, 8)
                term.Char($20)
            else
                data := long[adr]
                'data := libi(data)
                adr += 4
                term.hex(data, 8)
                term.Char($20)
        ifnot(lock_id == -1)
            lockclr(lock_id)


PRI libi(data) : retval
    retval.byte[0] := data.byte[3]
    retval.byte[1] := data.byte[2]
    retval.byte[2] := data.byte[1]
    retval.byte[3] := data.byte[0]
    return

DAT
CLR             byte    $1b, 12, 0
