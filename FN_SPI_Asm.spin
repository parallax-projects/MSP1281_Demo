{{
************************************************
* Propeller SPI Engine                    v0.1 *
* Author: Forthnutter                          *
* Copyright (c) 2023                           *
************************************************

Revision History:
         V0.1   - the begining
}}
CON
    CON_DUMP = 0
    CON_DUMPLOCK = 1
    CON_LOCK = 2
    CON_CMD = 3
    CON_CDAT = 4
    CON_QWHEAD = 5
    CON_QWTAIL = 6
    CON_CS = 7
    CON_DS = 8
    CON_MOSI = 9
    CON_MISO = 10
    CON_CLK = 11
    CON_RST = 12
    CON_BLK = 13
    CON_QWDATA = 14

    DS_START = 0
    DS_TEST = 1
    DS_END = 2

    SIZEOFQWDATA = 16

    VALID = 31
    COMDATA = 30

VAR
    long dump
    long dump_lock              ' dump lock number
    long lock_id                ' lock number
    long command                ' command code
    long cdata                  ' command data
    long wr_head                ' que suff
    long wr_tail
    long cs_pin                 ' number of the chip select pin
    long ds_pin                 ' number of the data select pin
    long mosi_pin               ' number of the master out slave in pin
    long miso_pin               ' number of the master in slave out pin
    long clk_pin                '
    long reset_pin              '
    long blk_pin
    long wr_data[SIZEOFQWDATA]
    long dump_mem[512]
    long cog
    long wr_end
    long wr_ihead

OBJ
    dumper : "dump"


'------------------------------------------------------------------------------------------------------------------------------
PUB start(cs_data, ds_data, mo, mi, clk, rst, blk) : retval | kcol
    retval := 0

    dump_lock := dumper.start(@dump_mem)
    dump := @dump_mem
    cs_pin := cs_data
    ds_pin := ds_data
    mosi_pin := mo
    miso_pin := mi
    clk_pin := clk
    reset_pin := rst
    blk_pin := blk
    command := 0
    cdata := 0
    wr_head := @wr_data                                 ' basic write queue
    wr_tail := @wr_data                                 ' tail of queue
    wr_end := @wr_data[SIZEOFQWDATA]      ' end address of queue
    wr_ihead := 0                               ' index head

    kcol := locknew
    ifnot(kcol == -1)
        lock_id := kcol
        cog := cognew(@spi_exe, @dump)
        ifnot(cog == -1)
            retval := @spi_exe
        else
            lockret(lock_id)    ' cannot get a cog so return the lock
    return

PUB get_asm : retval
    retval := @spi_exe

' cmd is the 32bit command and flags
' data is pointer to an array of dataA
PUB cmd_write(c, d) : retval
    retval := FALSE
    ifnot(lock_id == -1)
        repeat until not lockset(lock_id)
        command := c
        cdata := d
        lockclr(lock_id)
        retval := TRUE
    return

' put 32 bit data into the que
' result true if good
PUB que_put(data)
    result := FALSE

    ifnot(lock_id == -1)
        'repeat until not lockset(lock_id)
        ifnot(lockset(lock_id) == TRUE)
            wr_data[wr_ihead] := data
            wr_ihead += 1
            ifnot(wr_ihead < SIZEOFQWDATA)
                wr_ihead := 0
            wr_head := @wr_data[wr_ihead]
            lockclr(lock_id)
            result := TRUE

' put 32 bit data into the que
' result true if good
PUB que_command_put(data)
    data |= |< VALID            ' this indicates valid
    result := que_put(data)


PUB que_command_wait(data)
    repeat until que_command_put(data)


PUB que_data_put(data)
    data |= |< COMDATA
    data |= |< VALID
    result := que_put(data)


PUB que_data_wait(data)
    repeat until que_data_put(data)

DAT
                org
'
'' SPI Engine - main loop
'
spi_exe

                call    #stack_init                      ' make sure our data stack is initialised

                mov     p1, par                         'get first parameter 0
                rdlong  dumpmem, p1                     'dump routine gets started here
                add     p1, #4
                rdlong  dumplock, p1

                add     p1, #4                          'lock Id
                rdlong  lockid, p1

                add     p1, #4
                mov     acmd, p1                         ' we only want the address

                add     p1, #4
                mov     adata, p1                        ' we only want the address

                add     p1, #4
                mov     que_wr_head, p1                 ' we only want the address

                add     p1, #4
                mov     que_wr_tail, p1

                mov     W, #CON_CS                      ' Chip Select
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     cs, W

                mov     W, #CON_DS                       ' data select pin
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     cd, W

                mov     W, #CON_MOSI                     ' Master Out go to Third parameter
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     mosi, W                      ' data MOSI pin

                mov     W, #CON_MISO                     ' go to Master in parameter
                call    #PUSHW                        ' this should be serial in
                call    #GetPinmask
                call    #POPW
                mov     miso, W                       ' data miso pin

                mov     W, #CON_CLK                   ' go to Clock parameter
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     clk, W                       ' data clock pin

                mov     W, #CON_RST                         ' Reset Pin
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     rst, W                      ' data reset pin

                mov     W, #CON_BLK                    ' Blank screen pin
                call    #PUSHW
                call    #GetPinmask
                call    #POPW
                mov     blk, W                       ' data blank pin

                mov     W, #CON_QWDATA
                shl     W, #2
                mov     que_wr_data, par
                add     que_wr_data, W                           ' next we get CS PIN

                mov     que_wr_end, que_wr_data         ' move the start of data
                add     que_wr_end, #SIZEOFQWDATA<<2
                'sub     que_wr_end, #1

                xor     troo, #0    NR,WC               ' clear carry flag
                muxnc   outa, blk                       ' blk is hi to enable display
                muxnc   dira, blk                       ' blk is output put
                muxnc   outa, cs                        ' cs is low to enable
                muxnc   dira, cs                        ' cs is output
                muxnc   outa, rst                       ' rst is low to reset so make it hi for now
                muxnc   dira, rst                       ' rst is output
                muxc    outa, cd                        ' dsel is low for command hi for data
                muxnc   dira, cd                        ' dsel output
                muxc    outa, mosi                      ' mosi is data
                muxnc   dira, mosi                      ' let make it output for now
                muxc    outa, clk                       ' clock is low
                muxnc   dira, clk                       ' clk output low
                muxc    outa, P26
                muxnc   dira, P26
                muxnc   outa, P27
                muxnc   dira, P27
                lockclr lockid                          ' just incase it is locked
                call    #dump_init
loop
                call    #P26_toggle
                call    #cque_get
        IF_NZ   call    #spi_send
                'call    #read_cmd                       ' get the data from hub memory
                'call    #dump_run                       ' this is mainly for debuging just dump the cog memory out to HUB
:lloop
                call    #dump_transfer
'        IF_NZ   jmp     #:lloop
                'mov     W, cmd                          ' W has the read cmd
                'call    #PUSHW                          ' push w to stack
                'call    #cmd_valid                      ' now test validity of command data
                'call    #POPW                           ' W now has return value command byte
                'cmp     W, #0       wz                  ' W = 0 the command is not valid
'        if_z    jmp     #loop                           '
'                call    #spi_command
                jmp     #loop




' Just copy 512 long cog memory out to HUB memory
' so the dump spin send it out to serial
dump_run
                lockset dumplock    wc
        if_c    jmp     #dump_run_ret
                mov     dumpcog, #spi_exe
                mov     dumpsrc, dumpmem
                mov     dumpcountd, dumpsize
dumpr
                movs    :dumpmod, dumpcog
                nop
:dumpmod        mov     dumpdest, dumpcog
                nop

                wrlong  dumpdest, dumpsrc
                add     dumpsrc, #4
                add     dumpcog, #1
                djnz    dumpcountd, #dumpr
                lockclr dumplock
dump_run_ret
                ret

' Just copy 512 long cog memory out to HUB memory
' so the dump spin send it out to serial
dump_init

                mov     dumpcog, #spi_exe
                mov     dumpsrc, dumpmem
                mov     dumpcountd, dumpsize
dump_init_ret   ret

dump_transfer
                movs    :dumpmod, dumpcog
                nop
:dumpmod        mov     dumpdest, dumpcog
                nop
:dumploop
                lockset dumplock    wc
        if_c    jmp     #:dumploop
                wrlong  dumpdest, dumpsrc
                lockclr dumplock
                add     dumpsrc, #4
                add     dumpcog, #1
                djnz    dumpcountd, #dump_transfer_ret
                call    #dump_init
dump_transfer_ret
                ret



' ( cmd -- )
spi_send
'               call    #P27_toggle
                'call    #dup
                call    #POPW
                test    W,CMDVALID  WC
        IF_NC   jmp     #spi_send_ret
                test    W,CMDADDRESS    WC  ' we test to see if addres bit is set
        IF_C    jmp     #spi_buffer
                test    W,CMDCD     WC
        IF_C    or      outa,cd
        IF_NC   andn    outa,cd
        IF_NC   call    #P27_toggle
                call    #cs_clr
:lloop          'call    #dump_transfer
                'jmp     #:lloop
                call    #PUSHW
                call    #spi_msb
                call    #cs_set
spi_send_ret    ret

spi_buffer
:lloop          call    #dump_transfer
                jmp     #:lloop
                jmp     #spi_send_ret


P26_toggle
                test    outa, P26   wz
        if_z    or      outa, P26
        if_nz   andn    outa, P26
P26_toggle_ret
                ret

P27_toggle
                test    outa, P27   wz
        if_z    or      outa, P27
        if_nz   andn    outa, P27
P27_toggle_ret
                ret


mosi_set        or      outa, mosi
mosi_set_ret    ret

mosi_clr        andn    outa, mosi
mosi_clr_ret    ret

clk_set         or      outa, clk
clk_set_ret     ret

clk_clr         andn    outa, clk
clk_clr_ret     ret

clk_toggle
                test    outa, clk   wz
        if_z    or      outa, clk
        if_nz   andn    outa, clk
        if_z    andn    outa, clk
        if_nz   or      outa, clk
clk_toggle_ret  ret


' set CS pin high
cs_set          or      outa, cs
cs_set_ret      ret

' clear CS pin low
cs_clr          andn    outa, cs
cs_clr_ret      ret

' set CD pin set
cd_set          or      outa, cd
cd_set_ret      ret

' clear CD pin low
cd_clr          andn    outa, cd
cd_clr_ret      ret

' ( -- cmd )
push_cmd        mov     W, cmd
                call    #PUSHW
push_cmd_ret    ret
'################################################################################################################
'tested OK
' lets just send data from stack out to spi
' ( dd -- )
spi_msb                                               ''SHIFTOUT Entry
                call    #POPW
                mov     p2, #8
                mov     p1, W       wz
                shl     p1, #24
:spi_loop
                shl     p1, #1      wc
        if_nc   call    #mosi_clr                      ''          PreSet DataPin LOW
        if_c    call    #mosi_set


                call    #clk_toggle                            ''          PreSet ClockPin LOW
                djnz    p2, #:spi_loop
spi_msb_ret     ret
'------------------------------------------------------------------------------------------------------------------------------
'
' documents talk about three clk bits before parameters or data
spi_three       mov     p2, #3

:spi_loop       shl     p1, #1
        if_nc   call    #mosi_clr
        if_c    call    #mosi_set
                call    #clk_toggle
                djnz    p2, #:spi_loop
spi_three_ret   ret





'************************************************************************
' QUEUE FUNCTIONS
'*************************************************************************

cque_test_address
                cmp     que_wr_data, #0 WZ
        IF_Z    jmp     #:cqtend
                cmp     que_wr_head, #0 WZ
        IF_Z    jmp     #:cqtend
                cmp     que_wr_tail, #0 WZ
        IF_Z    jmp     #:cqtend
                cmp     que_wr_end, #0  WZ
        IF_Z    jmp     #:cqtend
                shl     fuls, #1    NR,WC               ' clear carry
                jmp     #cque_test_address_ret
:cqtend         shl     troo, #1    NR,WC               ' carry set
cque_test_address_ret
                ret

' get long value from queue
' return Z = 1 if queue is empty
' return Z = 0 and data on stack
cque_get
                call    #cque_test_address
        IF_C    test    fuls, fuls  WZ                  ' this should set z
        IF_C    jmp     #cque_get_ret                   ' carry set from sub means error
                lockset lockid      wc
        IF_C    test    fuls,fuls   WZ
        IF_C    jmp     #cque_get_ret                   ' if carry set the lock is set some where else
                rdlong  A0, que_wr_head                 ' get the address of hub wr head
                rdlong  A1, que_wr_tail                 ' get the address of hub wr tail
                mov     A2, que_wr_end                  ' end address
                cmp     A0, A1      WZ                  ' if the same no data
        IF_Z    jmp     #:cqg
                rdlong  W, A1                           ' a1 tail pointer w has the read data
                call    #pushw
                add     A1, #4                          ' increment tail
                cmp     A1, A2      WC,WZ
        IF_NC   mov     A1, que_wr_data
                wrlong  A1, que_wr_tail
                xor     troo, #0    NR,WZ
:cqg            lockclr lockid
cque_get_ret    ret

' put data into the read quque so hub can read
' ( l -- )
' data on stack
'cque_put
'                rdlong  A0, que_rd_head                                         ' get the address of hub rd head
'                rdlong  A1, que_rd_tail                                         '
'                rdlong  A2, que_rd_end
'                call    #skcheck
'        IF_Z        jmp         #cque_put_ret
'                call    #popw
'                wrlong  W,A0
'                add     A0, #4
'                cmp     A0, A2                          WC,WZ
'    IF_A        mov     A0, que_rd_start
'                wrlong  A0,que_rd_head
'                xor     TRUE, #0            NR,WZ
'cque_put_ret    ret



'----------------------------------------------------------------------------------------------------------------
{ keep all the pretend forth here }

'----------------------------------------------------------------------------------------------------------------
' ( dd -- bb )
' get stack value and return byte 3
' ( ddxxxxxx -- dd )
b3              call    #POPW
                shr     W, #24          ' shift 24 - 31 to get 8 bit from top
                and     W, #$FF
                call    #PUSHW          ' w is now on stack
b3_ret          ret

'
' ( xxddxxxx -- dd )
b2              call    #POPW
                shr     W, #16          ' shift 16 - 23
                and     W, #$FF         ' make sure we have only 8 bits
                call    #PUSHW
b2_ret          ret

'
' ( xxxxddxx -- dd )
b1
                call    #POPW
                shr     W, #8           ' get 8 - 15
                and     W, #$FF
                call    #PUSHW
b1_ret          ret


'
' ( xxxxxxdd -- dd )
b0
                call    #POPW
                and     W, #$FF
                call    #PUSHW
b0_ret          ret



'------------------------------------------------------------------------------------------------------------------------------
' ( par -- dd )
' Parameter number spin need will meed to make variable into long
GetPinmask
                call    #POPW                           ' ( par -- W = par )
                shl     W, #2
                mov     A, par                         '
                add     A, W                           ' next we get CS PIN
                rdlong  W, A                           ' read second parameter should be cs_pin number
                mov     A, #1
                shl     A, W
                call    #PUSHA                         ' ( -- par dd )
GetPinmask_ret
                ret

'--------------------
' ( dd -- dd+1 )
inc
                call    #POPW
                add     W, #1
                call    #PUSHW
inc_ret         ret


{
        Stack routines
}


' read data from top of stack
tw
                movs    twmod, T                                ' set modify code
                nop
twmod           mov     W, T                                            ' get top value
tw_ret          ret


' ( n -- n n )
' duplicate top of stack
dup
                call    #tw
                call    #PUSHW
dup_ret         ret

'Fetch and push to --T and increment A
AR_Inc
                call    #AR
                add     A, #1
AR_Inc_ret      ret

' fetch the contents of Memory address by A into --T
AR
                movs    ARsmod, A
                nop
ARsmod          mov     W, A
                call    #PUSHW
AR_ret          ret

' push the contents of W into --T
PUSHW           sub     T, #1
                movd    pushwsm, T
                nop
pushwsm         mov     T, W
                add     DC, #1
                mov     S, T
                sub     S, #1
PUSHW_ret       ret

' push the contents of A to --T
PUSHA
                sub     T, #1                                           ' decrement to put data into stack
                movd    pushamod, T                               ' setup self modifying code
                nop                                                             ' pipes need cleaning
pushamod        mov     T, A                                            ' put W into the top of stack
                add     DC, #1
                mov     S, T
                sub     S, #1
PUSHA_ret       ret

' data stack pop
' W has the data from stack
POPW
                movs    popwsmod, T                               ' self modify code
                nop                                                             ' nop is need for pipes to work
popwsmod        mov     W, T                                            ' place holder for self modifying code
                add     T, #1                                           ' increment to the next stack location
                sub     DC, #1
                mov     S, T
                sub     S, #1
POPW_ret        ret

' data stack pop
' W has the data from stack
POPA
                movs    popasmod, T                               ' self modify code
                nop                                                             ' nop is need for pipes to work
popasmod        mov     A, T                                            ' place holder for self modifying code
                add     T, #1                                           ' increment to the next stack location
                sub     DC, #1
                mov     S, T
                sub     S, #1
POPA_ret        ret


' Initialise T to the top of data stack
'
stack_init
                mov     T, #DS
                add     T, #16-1
                mov     DC, #0
                mov     S, T
                sub     S, #1                           'secon element on stack
                mov     R, #RS
                add     R, #16-1
stack_init_ret
                ret


{
########################### Assembly variables ###########################
}



zero            long    0                               ''constants
'd0              long    $200
troo            long    $FFFFFFFF
fuls            long    $00000000

                                                        ''temp variables
t1              long    0                               ''     Used for DataPin mask     and     COG shutdown
t2              long    0                               ''     Used for CLockPin mask    and     COG shutdown
t3              long    0                               ''     Used to hold DataValue SHIFTIN/SHIFTOUT
t4              long    0                               ''     Used to hold # of Bits
t5              long    0                               ''     Used for temporary data mask
t6              long    0                               ''     Used for Clock Delay
address         long    0                               ''     Used to hold return address of first Argument passed

'arg0            long    0                               ''arguments passed to/from high-level Spin
'arg1            long    0
'arg2            long    0
'arg3            long    0
'arg4            long    0

'
'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
P26             long    %00000100_00000000_00000000_00000000    ' P26
P27             long    %00001000_00000000_00000000_00000000    ' P27

'                        VCA
'                        IDD
'                        CONTROL  NUMBYTES RESERVED COMMAND
'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
cmd             long    %00000000_00000000_00000000_00000000
CMDVALID        long    %10000000_00000000_00000000_00000000
CMDCD           long    %01000000_00000000_00000000_00000000
CMDADDRESS      long    %00100000_00000000_00000000_00000000
'                       VI = H valid data
'                       CD = Command or data
'                       AD = we have 16 bit HUB address and NUMBYTES is 23-16 large data transfer
'






' QUEUE variables
que_wr_head     long    0
que_wr_tail     long    0
que_wr_data     long    0
que_wr_end      long    0


' debug via image dumping the cog memory
dumpsize        long    512
' holder for dump memory address
dumpmem         long    0
dumplock        long    -1
dumpcog         long    0
dumpsrc         long    0
dumpcountd      long    0
dumpdest        long    0

' SPI registers
acmd            long    0                               'Addess of command in spin world
adata           long    0                               'Address of data in the spin world
lockid          long    -1                              ' lock id

numbytes        long    0
count           long    0
' pin config for display
cs              long    0                               ' chip select pin
cd              long    0                               ' data select pin mainly for displays
rst             long    0                               ' reset pin
blk             long    0                               ' blk pin
mosi            long    0                               ' Master out slave in pin bit location
miso            long    0                               ' Master in slave out pin bit location
clk             long    0



' general parameters
p1              res     1                               ' parameter variable
p2              res     1

' tiny machine forth registers
W               res     1                               ' working register
A               res     1                               ' address register
A0              res     1
A1              res     1
A2              res     1
T               res     1                               ' top of data stack
S               res     1                               ' sub top
R               res     1                               ' return stack might not use
DC              res     1                               ' data stack count
DS              res     8                              ' 16 element data stake
RS              res     8                              ' 16 element return stack

                fit
