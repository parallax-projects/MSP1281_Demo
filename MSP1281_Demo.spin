CON

    _XINFREQ = 5_000_000
    _CLKMODE = XTAL1 + PLL16X
    _STACK = 3000

    MAX_LINE = 40
VAR
    byte line[MAX_LINE]

OBJ
'    term            : "com.serial.terminal"
'    commandparser   : "commandparser"
'    str             : "string"
'    num             : "string.integer"
'    dumper : "dump"
    msp1281 : "MSP1281_Driver"

PUB start | hex

    ' initialization
    outa[15] := TRUE
    dira[15] := TRUE

    msp1281.start

    waitcnt(10000000 + cnt)

    'msp1281.testing
    repeat 1
        ' loop
        'Triangle
        waitcnt(80000000 + cnt)
        outa[15] := FALSE
'        msp1281.testing
        outa[15] := TRUE
