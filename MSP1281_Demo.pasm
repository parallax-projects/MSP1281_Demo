con
	_clkfreq = 80000000
	_clkmode = 1032
' 
'     _XINFREQ = 5_000_000
	_XINFREQ = 5000000
'     _CLKMODE = XTAL1 + PLL16X
' 
'     MAX_LINE = 40
	MAX_LINE = 40
pub main
  coginit(0, @entry, 0)
dat
	org	0
entry
	mov	arg01, par wz
 if_ne	jmp	#spininit
	mov	__pc, $+2
	call	#LMM_CALL_FROM_COG
	long	@@@_start
cogexit
	cogid	arg01
	cogstop	arg01
spininit
	mov	sp, arg01
	rdlong	objptr, sp
	add	sp, #4
	rdlong	__pc, sp
	wrlong	ptr_hubexit_, sp
	add	sp, #4
	rdlong	arg01, sp
	add	sp, #4
	rdlong	arg02, sp
	add	sp, #4
	rdlong	arg03, sp
	add	sp, #4
	rdlong	arg04, sp
	sub	sp, #12
	jmp	#LMM_LOOP
LMM_LOOP
    rdlong LMM_i1, __pc
    add    __pc, #4
LMM_i1
    nop
    rdlong LMM_i2, __pc
    add    __pc, #4
LMM_i2
    nop
    rdlong LMM_i3, __pc
    add    __pc, #4
LMM_i3
    nop
    rdlong LMM_i4, __pc
    add    __pc, #4
LMM_i4
    nop
    rdlong LMM_i5, __pc
    add    __pc, #4
LMM_i5
    nop
    rdlong LMM_i6, __pc
    add    __pc, #4
LMM_i6
    nop
    rdlong LMM_i7, __pc
    add    __pc, #4
LMM_i7
    nop
    rdlong LMM_i8, __pc
    add    __pc, #4
LMM_i8
    nop
LMM_jmptop
    jmp    #LMM_LOOP
__pc
    long @@@hubentry
__lr
    long 0
hubretptr
    long @@@hub_ret_to_cog
LMM_NEW_PC
    long   0
    ' fall through
LMM_CALL
    rdlong LMM_NEW_PC, __pc
    add    __pc, #4
LMM_CALL_PTR
    wrlong __pc, sp
    add    sp, #4
LMM_JUMP_PTR
    mov    __pc, LMM_NEW_PC
    jmp    #LMM_LOOP
LMM_JUMP
    rdlong __pc, __pc
    jmp    #LMM_LOOP
LMM_RET
    sub    sp, #4
    rdlong __pc, sp
    jmp    #LMM_LOOP
LMM_CALL_FROM_COG
    wrlong  hubretptr, sp
    add     sp, #4
    jmp  #LMM_LOOP
LMM_CALL_FROM_COG_ret
    ret
    
LMM_CALL_ret
LMM_CALL_PTR_ret
LMM_JUMP_ret
LMM_JUMP_PTR_ret
LMM_RET_ret
LMM_RA
    long	0
    
LMM_FCACHE_LOAD
    rdlong FCOUNT_, __pc
    add    __pc, #4
    mov    ADDR_, __pc
    sub    LMM_ADDR_, __pc
    tjz    LMM_ADDR_, #a_fcachegoaddpc
    movd   a_fcacheldlp, #LMM_FCACHE_START
    shr    FCOUNT_, #2
a_fcacheldlp
    rdlong 0-0, __pc
    add    __pc, #4
    add    a_fcacheldlp,inc_dest1
    djnz   FCOUNT_,#a_fcacheldlp
    '' add in a JMP back out of LMM
    ror    a_fcacheldlp, #9
    movd   a_fcachecopyjmp, a_fcacheldlp
    rol    a_fcacheldlp, #9
a_fcachecopyjmp
    mov    0-0, LMM_jmptop
a_fcachego
    mov    LMM_ADDR_, ADDR_
    jmpret LMM_RETREG,#LMM_FCACHE_START
a_fcachegoaddpc
    add    __pc, FCOUNT_
    jmp    #a_fcachego
LMM_FCACHE_LOAD_ret
    ret
inc_dest1
    long (1<<9)
LMM_LEAVE_CODE
    jmp LMM_RETREG
LMM_ADDR_
    long 0
ADDR_
    long 0
FCOUNT_
    long 0
COUNT_
    long 0
prcnt_
    long 0
pushregs_
      movd  :write, #local01
      mov   prcnt_, COUNT_ wz
  if_z jmp  #pushregs_done_
:write
      wrlong 0-0, sp
      add    :write, inc_dest1
      add    sp, #4
      djnz   prcnt_, #:write
pushregs_done_
      wrlong COUNT_, sp
      add    sp, #4
      wrlong fp, sp
      add    sp, #4
      mov    fp, sp
pushregs__ret
      ret
popregs_
      sub   sp, #4
      rdlong fp, sp
      sub   sp, #4
      rdlong COUNT_, sp wz
  if_z jmp  #popregs__ret
      add   COUNT_, #local01
      movd  :read, COUNT_
      sub   COUNT_, #local01
:loop
      sub    :read, inc_dest1
      sub    sp, #4
:read
      rdlong 0-0, sp
      djnz   COUNT_, #:loop
popregs__ret
      ret
' code originally from spin interpreter, modified slightly

unsdivide_
       mov     itmp2_,#0
       jmp     #udiv__

divide_
       abs     muldiva_,muldiva_     wc       'abs(x)
       muxc    itmp2_,divide_haxx_            'store sign of x (mov x,#1 has bits 0 and 31 set)
       abs     muldivb_,muldivb_     wc,wz    'abs(y)
 if_z  jmp     #divbyzero__
 if_c  xor     itmp2_,#1                      'store sign of y
udiv__
divide_haxx_
        mov     itmp1_,#1                    'unsigned divide (bit 0 is discarded)
        mov     DIVCNT,#32
mdiv__
        shr     muldivb_,#1        wc,wz
        rcr     itmp1_,#1
 if_nz   djnz    DIVCNT,#mdiv__
mdiv2__
        cmpsub  muldiva_,itmp1_        wc
        rcl     muldivb_,#1
        shr     itmp1_,#1
        djnz    DIVCNT,#mdiv2__
        shr     itmp2_,#31       wc,wz    'restore sign
        negnz   muldiva_,muldiva_         'remainder
        negc    muldivb_,muldivb_ wz      'division result
divbyzero__
divide__ret
unsdivide__ret
	ret
DIVCNT
	long	0

entryptr__
	long	@@@entry
fp
	long	0
imm_1000000000_
	long	1000000000
imm_100000000_
	long	100000000
imm_1032_
	long	1032
imm_115200_
	long	115200
imm_16711680_
	long	16711680
imm_16777215_
	long	16777215
imm_2088_
	long	2088
imm_2147483648_
	long	-2147483648
imm_3228_
	long	3228
imm_32768_
	long	32768
imm_4294967040_
	long	-256
imm_4294967295_
	long	-1
imm_512_
	long	512
imm_65532_
	long	65532
itmp1_
	long	0
itmp2_
	long	0
objptr
	long	@@@objmem
ptr__FN_SPI_Asm_dat__
	long	@@@_FN_SPI_Asm_dat_
ptr__FullDuplexSerial_dat__
	long	@@@_FullDuplexSerial_dat_
ptr__dump_dat__
	long	@@@_dump_dat_
ptr__dump_dumpspin_
	long	@@@_dump_dumpspin
ptr_hubexit_
	long	@@@hubexit
result1
	long	0
sp
	long	@@@stackspace
COG_BSS_START
	fit	496
hub_ret_to_cog
	jmp	#LMM_CALL_FROM_COG_ret
hubentry

' 
' PUB start | hex
_start
	mov	COUNT_, #2
	call	#pushregs_
' 
'     ' initialization
'     outa[15]~
	mov	local01, outa
	andn	local01, imm_32768_
	mov	outa, local01
'     dira[15]~~
	mov	local01, dira
	or	local01, imm_32768_
	mov	dira, local01
'     dumper.start(msp1281.start)
	add	objptr, imm_3228_
	call	#LMM_CALL
	long	@@@_MSP1281_Driver_start
	sub	objptr, imm_3228_
	mov	local01, result1
	mov	arg01, local01
	add	objptr, imm_2088_
	call	#LMM_CALL
	long	@@@_dump_start
	sub	objptr, imm_2088_
' 
' 
'     waitcnt(1000000000 + cnt)
	mov	local02, imm_1000000000_
	add	local02, cnt
	mov	local01, local02
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system__waitcnt
' 
'     msp1281.test
	add	objptr, imm_3228_
	call	#LMM_CALL
	long	@@@_MSP1281_Driver_test
	sub	objptr, imm_3228_
'     repeat
LR__0001
'         ' loop
'         'Triangle
'         waitcnt(100000000 + cnt)
	mov	local02, imm_100000000_
	add	local02, cnt
	mov	local01, local02
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system__waitcnt
'         outa[15]~~
	mov	local01, outa
	or	local01, imm_32768_
	mov	outa, local01
'         'msp1281.test
'         outa[15]~
	mov	local01, outa
	andn	local01, imm_32768_
	mov	outa, local01
	rdlong	__pc,__pc
	long	@@@LR__0001
	mov	sp, fp
	call	#popregs_
_start_ret
	call	#LMM_RET

' '    commandparser   : "commandparser"
' '    str             : "string"
' '    num             : "string.integer"
' 
' 
' PUB start(address)
_dump_start
	mov	COUNT_, #8
	call	#pushregs_
	mov	local01, arg01
'         ' initialization
'         'term.Start (115200)
'         term.start_default (115200)
	mov	local02, imm_115200_
	mov	arg01, local02
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_start_default
	sub	objptr, imm_1032_
'         
'         term.Hex(address,5)
	mov	local02, local01
	mov	local03, #5
	mov	arg01, local02
	mov	arg02, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_hex
	sub	objptr, imm_1032_
'         term.nl
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_nl
	sub	objptr, imm_1032_
'         cog_address := address
	add	objptr, #4
	wrlong	local01, objptr
	sub	objptr, #4
'         cog_id := cognew(dumpspin(address),@dumpstack)
	mov	local02, objptr
	add	objptr, #8
	mov	local03, objptr
	sub	objptr, #8
	wrlong	local02, local03
	add	local03, #4
	wrlong	ptr__dump_dumpspin_, local03
	add	local03, #4
	mov	local04, local01
	wrlong	local04, local03
	add	local03, #4
	mov	local05, #30
	mov	local06, entryptr__
	add	objptr, #8
	mov	local07, objptr
	sub	objptr, #8
	mov	local08, local07
	mov	arg01, local05
	mov	arg02, local06
	mov	arg03, local08
	call	#LMM_CALL
	long	@@@__system___coginit
	mov	local05, result1
	wrlong	local05, objptr
	mov	sp, fp
	call	#popregs_
_dump_start_ret
	call	#LMM_RET

' 
' 
' PRI dumpspin(address) | index, adr, data
_dump_dumpspin
	mov	COUNT_, #6
	call	#pushregs_
	mov	local01, arg01
' 
' 
' '    term.start_default (115200)
'     repeat
LR__0010
'         waitcnt(100000000 + cnt)
	mov	local02, imm_100000000_
	add	local02, cnt
	mov	local03, local02
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@__system__waitcnt
'         'term.Clear
'         term.Str(@CLR)
	mov	local02, ptr__dump_dat__
	mov	local03, local02
	mov	arg01, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_str
	sub	objptr, imm_1032_
'         adr := address
	mov	local04, local01
'         repeat index from 0 to 511
	mov	local05, #0
LR__0011
'             if(( index // 16) == 0)
	abs	local03, local05 wc
	and	local03, #15
	negc	local03, local03
	cmp	local03, #0 wz
 if_ne	rdlong	__pc,__pc
	long	@@@LR__0012
'                 term.nl
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_nl
	sub	objptr, imm_1032_
'                 term.Hex(adr, 5)
	mov	local03, local04
	mov	local02, #5
	mov	arg01, local03
	mov	arg02, local02
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_hex
	sub	objptr, imm_1032_
'                 term.tx($20)
	mov	local03, #32
	mov	arg01, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_tx
	sub	objptr, imm_1032_
'                 term.Hex(index, 3)
	mov	local03, local05
	mov	local02, #3
	mov	arg01, local03
	mov	arg02, local02
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_hex
	sub	objptr, imm_1032_
'                 term.tx($20) ' give me some space
	mov	local03, #32
	mov	arg01, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_tx
	sub	objptr, imm_1032_
'                 data := long[adr]
	rdlong	local06, local04
'                 'data := libi(data)
'                 adr += 4
	mov	local03, local04
	add	local03, #4
	mov	local04, local03
'                 term.hex(data,8)
	mov	local03, local06
	mov	local02, #8
	mov	arg01, local03
	mov	arg02, local02
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_hex
	sub	objptr, imm_1032_
'                 term.tx($20)
	mov	local03, #32
	mov	arg01, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_tx
	sub	objptr, imm_1032_
	rdlong	__pc,__pc
	long	@@@LR__0013
LR__0012
'             else
'                 data := long[adr]
	rdlong	local06, local04
'                 'data := libi(data)
'                 adr += 4
	mov	local03, local04
	add	local03, #4
	mov	local04, local03
'                 term.hex(data,8)
	mov	local03, local06
	mov	local02, #8
	mov	arg01, local03
	mov	arg02, local02
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_hex
	sub	objptr, imm_1032_
'                 term.tx($20)
	mov	local03, #32
	mov	arg01, local03
	add	objptr, imm_1032_
	call	#LMM_CALL
	long	@@@_std_text_routines_tx
	sub	objptr, imm_1032_
LR__0013
	mov	local03, local05
	mov	local02, local05
	add	local02, #1
	mov	local05, local02
	cmps	local05, imm_512_ wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0011
	rdlong	__pc,__pc
	long	@@@LR__0010
	mov	sp, fp
	call	#popregs_
_dump_dumpspin_ret
	call	#LMM_RET

' 
' 
' 
' 
' 
' 
' PUB start_default(baudrate)
_std_text_routines_start_default
	mov	COUNT_, #2
	call	#pushregs_
	mov	local01, arg01
'     term.start_default(baudrate)
	mov	local02, local01
	mov	arg01, local02
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_start_default
	sub	objptr, #32
	mov	local02, result1
	mov	sp, fp
	call	#popregs_
_std_text_routines_start_default_ret
	call	#LMM_RET

' 
' PUB tx(c)
_std_text_routines_tx
	mov	COUNT_, #2
	call	#pushregs_
	mov	local01, arg01
'     term.tx(c)
	mov	local02, local01
	mov	arg01, local02
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
	mov	sp, fp
	call	#popregs_
_std_text_routines_tx_ret
	call	#LMM_RET

' 
' {{
' }}
' 
' '' output a string
' pub str(s) | c
_std_text_routines_str
	mov	COUNT_, #5
	call	#pushregs_
	mov	local01, arg01
'   repeat while ((c := byte[s++]) <> 0)
LR__0020
	mov	local02, local01
	mov	local03, local01
	add	local03, #1
	mov	local01, local03
	rdbyte	local04, local02
	mov	local05, local04
	cmp	local04, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0021
'     term.tx(c)
	mov	local02, local05
	mov	arg01, local02
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
	rdlong	__pc,__pc
	long	@@@LR__0020
LR__0021
	mov	sp, fp
	call	#popregs_
_std_text_routines_str_ret
	call	#LMM_RET

' 
' 
' 
' pub num(val, base, signflag, digitsNeeded) | i, digit, r1, q1
_std_text_routines_num
	mov	COUNT_, #12
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, arg03
	mov	local04, arg04
' 
'   '' if signflag is nonzero, it indicates we should treat
'   '' val as signed; if it is > 1, it is a character we should
'   '' print for positive numbers (typically "+")
' 
'   if (signflag)
	cmp	local03, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0031
'       if (val < 0)
	cmps	local01, #0 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0030
'         signflag := "-"
	mov	local03, #45
'         val := -val
	mov	local05, local01
	neg	local05, local05
	mov	local01, local05
LR__0030
LR__0031
' 
'   '' make sure we will not overflow our buffer
'   if (digitsNeeded > 32)
	cmps	local04, #33 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0032
'     digitsNeeded := 32
	mov	local04, #32
LR__0032
' 
'   '' accumulate the digits
'   i := 0
	mov	local06, #0
'   repeat
LR__0033
'     if (val < 0)
	cmps	local01, #0 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0035
'       ' synthesize unsigned division from signed
'       ' basically shift val right by 2 to make it positive
'       ' then adjust the result afterwards by the bit we
'       ' shifted out
'       r1 := val&1  ' capture low bit
	mov	local05, local01
	and	local05, #1
	mov	local07, local05
'       q1 := val>>1 ' divide val by 2
	mov	local05, local01
	shr	local05, #1
	mov	local08, local05
'       digit := r1 + 2*(q1 // base)
	mov	local05, local07
	mov	muldiva_, local08
	mov	muldivb_, local02
	call	#divide_
	mov	local09, muldiva_
	mov	local10, local09
	shl	local10, #1
	add	local05, local10
	mov	local11, local05
'       val := 2*(q1 / base)
	mov	local05, muldivb_
	mov	local09, local05
	shl	local09, #1
	mov	local01, local09
'       if (digit => base)
	cmps	local11, local02 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0034
'         val++
	mov	local05, local01
	mov	local09, local01
	add	local09, #1
	mov	local01, local09
'         digit -= base
	mov	local05, local11
	sub	local05, local02
	mov	local11, local05
LR__0034
	rdlong	__pc,__pc
	long	@@@LR__0036
LR__0035
'     else
'       digit := val // base
	mov	muldiva_, local01
	mov	muldivb_, local02
	call	#divide_
	mov	local05, muldiva_
	mov	local11, local05
'       val := val / base
	mov	local05, muldivb_
	mov	local01, local05
LR__0036
' 
'     if (digit => 0 and digit =< 9)
	cmps	local11, #0 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0037
	cmps	local11, #10 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0037
'        digit += "0"
	mov	local05, local11
	add	local05, #48
	mov	local11, local05
	rdlong	__pc,__pc
	long	@@@LR__0038
LR__0037
'     else
'        digit := (digit - 10) + "A"
	mov	local05, local11
	sub	local05, #10
	add	local05, #65
	mov	local11, local05
LR__0038
'     buf[i++] := digit
	mov	local05, local06
	mov	local09, local06
	add	local09, #1
	mov	local06, local09
	mov	local10, local05
	mov	local12, objptr
	add	local10, local12
	wrbyte	local11, local10
'     --digitsNeeded
	mov	local05, local04
	sub	local05, #1
	mov	local04, local05
	cmp	local01, #0 wz
 if_ne	rdlong	__pc,__pc
	long	@@@LR__0039
	cmps	local04, #1 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0039
 if_b	rdlong	__pc,__pc
	long	@@@LR__0040
LR__0039
	cmps	local06, #32 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0033
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0040
LR__0040
'   while (val <> 0 or digitsNeeded > 0) and (i < 32)
'   if (signflag > 1)
	cmps	local03, #2 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0041
'     term.tx(signflag)
	mov	local05, local03
	mov	arg01, local05
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
LR__0041
' 
'   '' now print the digits in reverse order
'   repeat while (i > 0)
LR__0042
	cmps	local06, #1 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0043
'     term.tx(buf[--i])
	mov	local09, local06
	sub	local09, #1
	mov	local06, local09
	mov	local10, local06
	mov	local12, objptr
	add	local10, local12
	rdbyte	local05, local10
	mov	arg01, local05
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
	rdlong	__pc,__pc
	long	@@@LR__0042
LR__0043
	mov	sp, fp
	call	#popregs_
_std_text_routines_num_ret
	call	#LMM_RET

' 
' '' print a hex number with the specified number
' '' of digits; 0 means just use as many as we need
' pub hex(val, digits = 8) | mask
_std_text_routines_hex
	mov	COUNT_, #7
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
'   if digits > 0 and digits < 8
	cmps	local02, #1 wc
 if_b	rdlong	__pc,__pc
	long	@@@LR__0050
	cmps	local02, #8 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0050
'     mask := (|< (4*digits)) - 1
	mov	local03, local02
	shl	local03, #2
	mov	local04, #1
	shl	local04, local03
	sub	local04, #1
	mov	local05, local04
'     val &= mask
	mov	local04, local01
	and	local04, local05
	mov	local01, local04
LR__0050
'   num(val, 16, 0, digits)
	mov	local04, local01
	mov	local03, #16
	mov	local06, #0
	mov	local07, local02
	mov	arg01, local04
	mov	arg02, local03
	mov	arg03, local06
	mov	arg04, local07
	call	#LMM_CALL
	long	@@@_std_text_routines_num
	mov	sp, fp
	call	#popregs_
_std_text_routines_hex_ret
	call	#LMM_RET

' 
' '' print a newline
' pub nl
_std_text_routines_nl
	mov	COUNT_, #1
	call	#pushregs_
'   term.tx(13)
	mov	local01, #13
	mov	arg01, local01
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
'   term.tx(10)
	mov	local01, #10
	mov	arg01, local01
	add	objptr, #32
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_tx
	sub	objptr, #32
	mov	sp, fp
	call	#popregs_
_std_text_routines_nl_ret
	call	#LMM_RET

' 
' PUB start(rxpin, txpin, mode, baudrate) : okay
_FullDuplexSerial_start
	mov	COUNT_, #11
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, arg03
	mov	local04, arg04
	mov	local05, #0
' 
' '' Start serial driver - starts a cog
' '' returns false if no cog available
' ''
' '' mode bit 0 = invert rx
' '' mode bit 1 = invert tx
' '' mode bit 2 = open-drain/source tx
' '' mode bit 3 = ignore tx echo on rx
' 
'   stop
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_stop
'   txlock := locknew
	call	#LMM_CALL
	long	@@@__system___locknew
	mov	local06, result1
	add	objptr, #72
	wrbyte	local06, objptr
	sub	objptr, #72
'   strlock := locknew
	call	#LMM_CALL
	long	@@@__system___locknew
	mov	local06, result1
	add	objptr, #73
	wrbyte	local06, objptr
	sub	objptr, #73
'   longfill(@rx_head, 0, 4)
	add	objptr, #4
	mov	local07, objptr
	sub	objptr, #4
	mov	local06, local07
	mov	local07, #0
	mov	local08, #4
	mov	arg01, local06
	mov	arg02, local07
	mov	arg03, local08
	call	#LMM_CALL
	long	@@@__system__longfill
'   longmove(@rx_pin, @rxpin, 3)
	add	objptr, #20
	wrlong	local01, objptr
	sub	objptr, #20
	add	objptr, #24
	wrlong	local02, objptr
	sub	objptr, #24
	add	objptr, #28
	wrlong	local03, objptr
	sub	objptr, #28
'   bit_ticks := clkfreq / baudrate
	rdlong	muldiva_, #0
	mov	muldivb_, local04
	call	#divide_
	mov	local06, muldivb_
	add	objptr, #32
	wrlong	local06, objptr
	sub	objptr, #32
'   buffer_ptr := @rx_buffer
	add	objptr, #40
	mov	local06, objptr
	sub	objptr, #40
	add	objptr, #36
	wrlong	local06, objptr
	sub	objptr, #36
'   okay := cog := cognew(@entry, @rx_head) + 1
	mov	local07, #30
	mov	local09, ptr__FullDuplexSerial_dat__
	mov	local08, local09
	add	objptr, #4
	mov	local10, objptr
	sub	objptr, #4
	mov	local09, local10
	mov	arg01, local07
	mov	arg02, local08
	mov	arg03, local09
	call	#LMM_CALL
	long	@@@__system___coginit
	mov	local07, result1
	mov	local06, local07
	add	local06, #1
	mov	local11, local06
	wrlong	local11, objptr
	mov	local05, local11
	mov	result1, local05
	rdlong	__pc,__pc
	long	@@@LR__0060
LR__0060
	mov	sp, fp
	call	#popregs_
_FullDuplexSerial_start_ret
	call	#LMM_RET

' 
' '' start with default pins
' PUB start_default(baudrate)
_FullDuplexSerial_start_default
	mov	COUNT_, #5
	call	#pushregs_
	mov	local01, arg01
'   return start(31, 30, 0, baudrate)
	mov	local02, #31
	mov	local03, #30
	mov	local04, #0
	mov	local05, local01
	mov	arg01, local02
	mov	arg02, local03
	mov	arg03, local04
	mov	arg04, local05
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_start
	mov	local02, result1
	mov	result1, local02
	rdlong	__pc,__pc
	long	@@@LR__0070
LR__0070
	mov	sp, fp
	call	#popregs_
_FullDuplexSerial_start_default_ret
	call	#LMM_RET

' 
' PUB stop
_FullDuplexSerial_stop
	mov	COUNT_, #4
	call	#pushregs_
' 
' '' Stop serial driver - frees a cog
' 
'   if cog
	rdlong	local01, objptr
	cmp	local01, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0080
'     cogstop(cog~ - 1)
	rdlong	local02, objptr
	mov	local03, #0
	wrlong	local03, objptr
	mov	local04, local02
	sub	local04, #1
	mov	local01, local04
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system___cogstop
	mov	local01, result1
'     lockret(txlock)
	add	objptr, #72
	rdbyte	local04, objptr
	sub	objptr, #72
	mov	local01, local04
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system___lockret
	mov	local01, result1
'     lockret(strlock)
	add	objptr, #73
	rdbyte	local04, objptr
	sub	objptr, #73
	mov	local01, local04
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system___lockret
	mov	local01, result1
LR__0080
'   longfill(@rx_head, 0, 9)
	add	objptr, #4
	mov	local04, objptr
	sub	objptr, #4
	mov	local01, local04
	mov	local04, #0
	mov	local03, #9
	mov	arg01, local01
	mov	arg02, local04
	mov	arg03, local03
	call	#LMM_CALL
	long	@@@__system__longfill
	mov	sp, fp
	call	#popregs_
_FullDuplexSerial_stop_ret
	call	#LMM_RET

' 
' PUB rxcheck : rxbyte
_FullDuplexSerial_rxcheck
	mov	_var01, #0
' 
' '' Check if byte received (never waits)
' '' returns -1 if no byte received, $00..$FF if byte
' 
'   rxbyte := -1
	mov	_var01, imm_4294967295_
'   if rx_tail <> rx_head
	add	objptr, #8
	rdlong	_var02, objptr
	sub	objptr, #8
	mov	_var03, _var02
	add	objptr, #4
	rdlong	_var04, objptr
	sub	objptr, #4
	mov	_var05, _var04
	cmp	_var03, _var05 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0090
'     rxbyte := rx_buffer[rx_tail]
	add	objptr, #8
	rdlong	_var02, objptr
	sub	objptr, #8
	mov	_var03, _var02
	add	objptr, #40
	mov	_var05, objptr
	sub	objptr, #40
	add	_var03, _var05
	rdbyte	_var01, _var03
'     rx_tail := (rx_tail + 1) & $F
	add	objptr, #8
	rdlong	_var02, objptr
	sub	objptr, #8
	mov	_var03, _var02
	add	_var03, #1
	and	_var03, #15
	add	objptr, #8
	wrlong	_var03, objptr
	sub	objptr, #8
LR__0090
	mov	result1, _var01
	rdlong	__pc,__pc
	long	@@@_FullDuplexSerial_rxcheck_ret
_FullDuplexSerial_rxcheck_ret
	call	#LMM_RET

' 
' 
' PUB rx : rxbyte
_FullDuplexSerial_rx
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, #0
' 
' '' Receive byte (may wait for byte)
' '' returns $00..$FF
' 
'   repeat while (rxbyte := rxcheck) < 0
LR__0100
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_rxcheck
	mov	local02, result1
	mov	local03, local02
	mov	local01, local03
	cmps	local03, #0 wc
 if_ae	rdlong	__pc,__pc
	long	@@@LR__0101
	rdlong	__pc,__pc
	long	@@@LR__0100
LR__0101
	mov	result1, local01
	rdlong	__pc,__pc
	long	@@@LR__0102
LR__0102
	mov	sp, fp
	call	#popregs_
_FullDuplexSerial_rx_ret
	call	#LMM_RET

' 
' 
' PUB tx(txbyte)
_FullDuplexSerial_tx
	mov	COUNT_, #5
	call	#pushregs_
	mov	local01, arg01
' 
' '' Send byte (may wait for room in buffer)
'   repeat while lockset(txlock)
LR__0110
	add	objptr, #72
	rdbyte	local02, objptr
	sub	objptr, #72
	mov	local03, local02
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@__system___lockset
	mov	local03, result1
	cmp	local03, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0111
	rdlong	__pc,__pc
	long	@@@LR__0110
LR__0111
'   repeat until (tx_tail <> (tx_head + 1) & $F)
LR__0112
	add	objptr, #12
	rdlong	local02, objptr
	sub	objptr, #12
	mov	local03, local02
	add	local03, #1
	and	local03, #15
	add	objptr, #16
	rdlong	local04, objptr
	sub	objptr, #16
	mov	local05, local04
	cmp	local05, local03 wz
 if_ne	rdlong	__pc,__pc
	long	@@@LR__0113
	rdlong	__pc,__pc
	long	@@@LR__0112
LR__0113
'   tx_buffer[tx_head] := txbyte
	add	objptr, #12
	rdlong	local02, objptr
	sub	objptr, #12
	mov	local03, local02
	add	objptr, #56
	mov	local05, objptr
	sub	objptr, #56
	add	local03, local05
	wrbyte	local01, local03
'   tx_head := (tx_head + 1) & $F
	add	objptr, #12
	rdlong	local02, objptr
	sub	objptr, #12
	mov	local03, local02
	add	local03, #1
	and	local03, #15
	add	objptr, #12
	wrlong	local03, objptr
	sub	objptr, #12
'   lockclr(txlock)
	add	objptr, #72
	rdbyte	local02, objptr
	sub	objptr, #72
	mov	local03, local02
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@__system___lockclr
	mov	local03, result1
' 
'   if rxtx_mode & %1000
	add	objptr, #28
	rdlong	local02, objptr
	sub	objptr, #28
	mov	local03, local02
	and	local03, #8
	cmp	local03, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0114
'     rx
	call	#LMM_CALL
	long	@@@_FullDuplexSerial_rx
	mov	local03, result1
LR__0114
	mov	sp, fp
	call	#popregs_
_FullDuplexSerial_tx_ret
	call	#LMM_RET

' 
' PUB start : retval | sdata, t
_MSP1281_Driver_start
	mov	COUNT_, #9
	call	#pushregs_
	mov	local01, #0
' 
' 
'     retval := spi.start(CSX,DCX, MOSI, MOSI,SCL,REST,BLK)
	mov	local02, #4
	mov	local03, #3
	mov	local04, #0
	mov	local05, #0
	mov	local06, #1
	mov	local07, #2
	mov	local08, #5
	mov	arg01, local02
	mov	arg02, local03
	mov	arg03, local04
	mov	arg04, local05
	mov	arg05, local06
	mov	arg06, local07
	mov	arg07, local08
	add	objptr, #16
	call	#LMM_CALL
	long	@@@_FN_SPI_Asm_start
	sub	objptr, #16
	mov	local02, result1
	mov	local01, local02
' 
'     'spi.cmd_write($aa55aa55, 0)
' 
'     ifnot(retval == 0)
	cmp	local01, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0120
'         sdata := 0
	mov	local09, #0
'         sdata.byte[0] := GC9A01A_INREGEN2
	mov	local02, local09
	and	local02, imm_4294967040_
	or	local02, #239
	mov	local09, local02
'         sdata.byte[2] := 0
	mov	local02, local09
	andn	local02, imm_16711680_
	mov	local09, local02
'         sdata.byte[3] := %1000_0000
	mov	local02, local09
	and	local02, imm_16777215_
	or	local02, imm_2147483648_
	mov	local09, local02
LR__0120
	mov	result1, local01
	rdlong	__pc,__pc
	long	@@@LR__0121
LR__0121
	mov	sp, fp
	call	#popregs_
_MSP1281_Driver_start_ret
	call	#LMM_RET

' 
' PUB test | sdata
_MSP1281_Driver_test
	mov	COUNT_, #3
	call	#pushregs_
'     sdata := 0
	mov	local01, #0
'     sdata.byte[0] := GC9A01A_INREGEN2
	mov	local02, local01
	and	local02, imm_4294967040_
	or	local02, #239
	mov	local01, local02
'     sdata.byte[2] := 0
	mov	local02, local01
	andn	local02, imm_16711680_
	mov	local01, local02
'     sdata.byte[3] := %1000_0000
	mov	local02, local01
	and	local02, imm_16777215_
	or	local02, imm_2147483648_
	mov	local01, local02
'     spi.cmd_write(sdata,0)
	mov	local02, local01
	mov	local03, #0
	mov	arg01, local02
	mov	arg02, local03
	add	objptr, #16
	call	#LMM_CALL
	long	@@@_FN_SPI_Asm_cmd_write
	sub	objptr, #16
	mov	local02, result1
	mov	sp, fp
	call	#popregs_
_MSP1281_Driver_test_ret
	call	#LMM_RET

' 
' 
' 
' '------------------------------------------------------------------------------------------------------------------------------
' PUB start(cs_data, ds_data, mo, mi, clk, rst, blk) : retval | kcol
_FN_SPI_Asm_start
	mov	COUNT_, #13
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, arg03
	mov	local04, arg04
	mov	local05, arg05
	mov	local06, arg06
	mov	local07, arg07
	mov	local08, #0
'     retval := 0
	mov	local08, #0
'     dump := @spi_exe
	mov	local09, ptr__FN_SPI_Asm_dat__
	wrlong	local09, objptr
'     cs_pin := cs_data
	add	objptr, #16
	wrlong	local01, objptr
	sub	objptr, #16
'     ds_pin := ds_data
	add	objptr, #20
	wrlong	local02, objptr
	sub	objptr, #20
'     mosi_pin := mo
	add	objptr, #24
	wrlong	local03, objptr
	sub	objptr, #24
'     miso_pin := mi
	add	objptr, #28
	wrlong	local04, objptr
	sub	objptr, #28
'     clk_pin := clk
	add	objptr, #32
	wrlong	local05, objptr
	sub	objptr, #32
'     reset_pin := rst
	add	objptr, #36
	wrlong	local06, objptr
	sub	objptr, #36
'     blk_pin := blk
	add	objptr, #40
	wrlong	local07, objptr
	sub	objptr, #40
'     command := 0
	mov	local09, #0
	add	objptr, #8
	wrlong	local09, objptr
	sub	objptr, #8
'     cdata := 0
	mov	local09, #0
	add	objptr, #12
	wrlong	local09, objptr
	sub	objptr, #12
'     kcol := locknew
	call	#LMM_CALL
	long	@@@__system___locknew
	mov	local09, result1
	mov	local10, local09
'     ifnot(kcol == -1)
	cmp	local10, imm_4294967295_ wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0132
'         lock_id := kcol
	add	objptr, #4
	wrlong	local10, objptr
	sub	objptr, #4
'         cog := cognew(@spi_exe, @dump)
	mov	local09, #30
	mov	local11, ptr__FN_SPI_Asm_dat__
	mov	local12, local11
	mov	local13, objptr
	mov	local11, local13
	mov	arg01, local09
	mov	arg02, local12
	mov	arg03, local11
	call	#LMM_CALL
	long	@@@__system___coginit
	mov	local09, result1
	add	objptr, #44
	wrlong	local09, objptr
	sub	objptr, #44
'         ifnot(cog == -1)
	add	objptr, #44
	rdlong	local12, objptr
	sub	objptr, #44
	mov	local09, local12
	cmp	local09, imm_4294967295_ wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0130
'             retval := @spi_exe
	mov	local09, ptr__FN_SPI_Asm_dat__
	mov	local08, local09
	rdlong	__pc,__pc
	long	@@@LR__0131
LR__0130
'         else
'             lockret(lock_id)    ' cannot get a cog so return the lock
	add	objptr, #4
	rdlong	local12, objptr
	sub	objptr, #4
	mov	local09, local12
	mov	arg01, local09
	call	#LMM_CALL
	long	@@@__system___lockret
	mov	local09, result1
LR__0131
LR__0132
	mov	result1, local08
	rdlong	__pc,__pc
	long	@@@LR__0133
LR__0133
	mov	sp, fp
	call	#popregs_
_FN_SPI_Asm_start_ret
	call	#LMM_RET

' 
' ' cmd is the 32bit command and flags
' ' data is pointer to an array of dataA
' PUB cmd_write(c, d) : retval
_FN_SPI_Asm_cmd_write
	mov	_var01, arg01
	mov	_var02, arg02
	mov	_var03, #0
'     retval := 0
	mov	_var03, #0
'     'ifnot(lock_id == -1)
' '        repeat until not lockset(lock_id)
'         test := 0 'command := 0
	mov	_var04, #0
	add	objptr, #48
	wrlong	_var04, objptr
	sub	objptr, #48
	mov	result1, _var03
	rdlong	__pc,__pc
	long	@@@_FN_SPI_Asm_cmd_write_ret
_FN_SPI_Asm_cmd_write_ret
	call	#LMM_RET
hubexit
	jmp	#cogexit

__system__waitcnt
	mov	_var01, arg01
	waitcnt	_var01, #0
__system__waitcnt_ret
	call	#LMM_RET

__system___cogstop
	mov	_var01, arg01
	cogstop	_var01
	mov	result1, #0
	rdlong	__pc,__pc
	long	@@@__system___cogstop_ret
__system___cogstop_ret
	call	#LMM_RET

__system___lockclr
	mov	_var01, arg01
	mov	_var02, imm_4294967295_
	lockclr	_var01 wc
	muxc	_var03, _var02
	mov	result1, _var03
	rdlong	__pc,__pc
	long	@@@__system___lockclr_ret
__system___lockclr_ret
	call	#LMM_RET

__system___lockset
	mov	_var01, arg01
	mov	_var02, imm_4294967295_
	lockset	_var01 wc
	muxc	_var03, _var02
	mov	result1, _var03
	rdlong	__pc,__pc
	long	@@@__system___lockset_ret
__system___lockset_ret
	call	#LMM_RET

__system___locknew
	mov	_var01, #0
	locknew	_var01
	mov	result1, _var01
	rdlong	__pc,__pc
	long	@@@__system___locknew_ret
	mov	result1, _var01
	rdlong	__pc,__pc
	long	@@@__system___locknew_ret
__system___locknew_ret
	call	#LMM_RET

__system___lockret
	mov	_var01, arg01
	lockret	_var01
	mov	result1, #0
	rdlong	__pc,__pc
	long	@@@__system___lockret_ret
__system___lockret_ret
	call	#LMM_RET

__system___coginit
	mov	_var01, arg01
	mov	_var02, arg02
	mov	_var03, arg03
	mov	_var04, _var03
	and	_var04, imm_65532_
	shl	_var04, #16
	mov	_var05, _var04
	mov	_var04, _var05
	mov	_var06, _var02
	and	_var06, imm_65532_
	shl	_var06, #2
	or	_var04, _var06
	mov	_var05, _var04
	mov	_var04, _var05
	mov	_var06, _var01
	and	_var06, #15
	or	_var04, _var06
	mov	_var05, _var04
	coginit	_var05 wc,wr
 if_b	neg	_var05, #1
	mov	result1, _var05
	rdlong	__pc,__pc
	long	@@@__system___coginit_ret
__system___coginit_ret
	call	#LMM_RET

__system____builtin_longset
	mov	_var01, arg01
	mov	_var02, arg02
	mov	_var03, arg03
	mov	_var04, #0
	mov	_var04, _var01
	mov	_var05, _var03
LR__0140
	cmp	_var05, #0 wz
 if_e	rdlong	__pc,__pc
	long	@@@LR__0142
	wrlong	_var02, _var01
	mov	_var06, _var01
	add	_var06, #4
	mov	_var01, _var06
LR__0141
	mov	_var06, _var05
	sub	_var06, #1
	mov	_var05, _var06
	rdlong	__pc,__pc
	long	@@@LR__0140
LR__0142
	mov	result1, _var04
	rdlong	__pc,__pc
	long	@@@__system____builtin_longset_ret
__system____builtin_longset_ret
	call	#LMM_RET

__system__longfill
	mov	COUNT_, #6
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, arg03
	mov	local04, local01
	mov	local05, local02
	mov	local06, local03
	mov	arg01, local04
	mov	arg02, local05
	mov	arg03, local06
	call	#LMM_CALL
	long	@@@__system____builtin_longset
	mov	local04, result1
	mov	sp, fp
	call	#popregs_
__system__longfill_ret
	call	#LMM_RET
	long
_dump_dat_
'-' CLR     byte    $1b,12,0
	byte	$1b, $0c, $00, $00
	long
_FullDuplexSerial_dat_
'-' 
'-' '***********************************
'-' '* Assembly language serial driver *
'-' '***********************************
'-' 
'-'                         org
'-' '
'-' '
'-' ' Entry
'-' '
'-' entry                   mov     t1,par                'get structure address
	byte	$f0, $ad, $bc, $a0
'-'                         add     t1,#4 << 2            'skip past heads and tails
	byte	$10, $ac, $fc, $80
'-' 
'-'                         rdlong  t2,t1                 'get rx_pin
	byte	$56, $ae, $bc, $08
'-'                         mov     rxmask,#1
	byte	$01, $b6, $fc, $a0
'-'                         shl     rxmask,t2
	byte	$57, $b6, $bc, $2c
'-' 
'-'                         add     t1,#4                 'get tx_pin
	byte	$04, $ac, $fc, $80
'-'                         rdlong  t2,t1
	byte	$56, $ae, $bc, $08
'-'                         mov     txmask,#1
	byte	$01, $c2, $fc, $a0
'-'                         shl     txmask,t2
	byte	$57, $c2, $bc, $2c
'-' 
'-'                         add     t1,#4                 'get rxtx_mode
	byte	$04, $ac, $fc, $80
'-'                         rdlong  rxtxmode,t1
	byte	$56, $b2, $bc, $08
'-' 
'-'                         add     t1,#4                 'get bit_ticks
	byte	$04, $ac, $fc, $80
'-'                         rdlong  bitticks,t1
	byte	$56, $b4, $bc, $08
'-' 
'-'                         add     t1,#4                 'get buffer_ptr
	byte	$04, $ac, $fc, $80
'-'                         rdlong  rxbuff,t1
	byte	$56, $b8, $bc, $08
'-'                         mov     txbuff,rxbuff
	byte	$5c, $c4, $bc, $a0
'-'                         add     txbuff,#16
	byte	$10, $c4, $fc, $80
'-' 
'-'                         test    rxtxmode,#%100  wz    'init tx pin according to mode
	byte	$04, $b2, $7c, $62
'-'                         test    rxtxmode,#%010  wc
	byte	$02, $b2, $7c, $61
'-'         if_z_ne_c       or      outa,txmask
	byte	$61, $e8, $9b, $68
'-'         if_z            or      dira,txmask
	byte	$61, $ec, $ab, $68
'-' 
'-'                         mov     txcode,#transmit      'initialize ping-pong multitasking
	byte	$34, $cc, $fc, $a0
'-'                         wrlong  zero,par              ' let user know COG started
	byte	$f0, $ab, $3c, $08
'-' '
'-' '
'-' ' Receive
'-' '
'-' receive                 jmpret  rxcode,txcode         'run a chunk of transmit code, then return
	byte	$66, $c0, $bc, $5c
'-' 
'-'                         test    rxtxmode,#%001  wz    'wait for start bit on rx pin
	byte	$01, $b2, $7c, $62
'-'                         test    rxmask,ina      wc
	byte	$f2, $b7, $3c, $61
'-'         if_z_eq_c       jmp     #receive
	byte	$17, $00, $64, $5c
'-' 
'-'                         mov     rxbits,#9             'ready to receive byte
	byte	$09, $bc, $fc, $a0
'-'                         mov     rxcnt,bitticks
	byte	$5a, $be, $bc, $a0
'-'                         shr     rxcnt,#1
	byte	$01, $be, $fc, $28
'-'                         add     rxcnt,cnt
	byte	$f1, $bf, $bc, $80
'-' 
'-' :bit                    add     rxcnt,bitticks        'ready next bit period
	byte	$5a, $be, $bc, $80
'-' 
'-' :wait                   jmpret  rxcode,txcode         'run a chuck of transmit code, then return
	byte	$66, $c0, $bc, $5c
'-' 
'-'                         mov     t1,rxcnt              'check if bit receive period done
	byte	$5f, $ac, $bc, $a0
'-'                         sub     t1,cnt
	byte	$f1, $ad, $bc, $84
'-'                         cmps    t1,#0           wc
	byte	$00, $ac, $7c, $c1
'-'         if_nc           jmp     #:wait
	byte	$20, $00, $4c, $5c
'-' 
'-'                         test    rxmask,ina      wc    'receive bit on rx pin
	byte	$f2, $b7, $3c, $61
'-'                         rcr     rxdata,#1
	byte	$01, $ba, $fc, $30
'-'                         djnz    rxbits,#:bit
	byte	$1f, $bc, $fc, $e4
'-' 
'-'                         shr     rxdata,#32-9          'justify and trim received byte
	byte	$17, $ba, $fc, $28
'-'                         and     rxdata,#$FF
	byte	$ff, $ba, $fc, $60
'-'                         test    rxtxmode,#%001  wz    'if rx inverted, invert byte
	byte	$01, $b2, $7c, $62
'-'         if_nz           xor     rxdata,#$FF
	byte	$ff, $ba, $d4, $6c
'-' 
'-'                         rdlong  t2,par                'save received byte and inc head
	byte	$f0, $af, $bc, $08
'-'                         add     t2,rxbuff
	byte	$5c, $ae, $bc, $80
'-'                         wrbyte  rxdata,t2
	byte	$57, $ba, $3c, $00
'-'                         sub     t2,rxbuff
	byte	$5c, $ae, $bc, $84
'-'                         add     t2,#1
	byte	$01, $ae, $fc, $80
'-'                         and     t2,#$0F
	byte	$0f, $ae, $fc, $60
'-'                         wrlong  t2,par
	byte	$f0, $af, $3c, $08
'-' 
'-'                         jmp     #receive              'byte done, receive next byte
	byte	$17, $00, $7c, $5c
'-' '
'-' '
'-' ' Transmit
'-' '
'-' transmit                jmpret  txcode,rxcode         'run a chunk of receive code, then return
	byte	$60, $cc, $bc, $5c
'-' 
'-'                         mov     t1,par                'check for head <> tail
	byte	$f0, $ad, $bc, $a0
'-'                         add     t1,#2 << 2
	byte	$08, $ac, $fc, $80
'-'                         rdlong  t2,t1
	byte	$56, $ae, $bc, $08
'-'                         add     t1,#1 << 2
	byte	$04, $ac, $fc, $80
'-'                         rdlong  t3,t1
	byte	$56, $b0, $bc, $08
'-'                         cmp     t2,t3           wz
	byte	$58, $ae, $3c, $86
'-'         if_z            jmp     #transmit
	byte	$34, $00, $68, $5c
'-' 
'-'                         add     t3,txbuff             'get byte and inc tail
	byte	$62, $b0, $bc, $80
'-'                         rdbyte  txdata,t3
	byte	$58, $c6, $bc, $00
'-'                         sub     t3,txbuff
	byte	$62, $b0, $bc, $84
'-'                         add     t3,#1
	byte	$01, $b0, $fc, $80
'-'                         and     t3,#$0F
	byte	$0f, $b0, $fc, $60
'-'                         wrlong  t3,t1
	byte	$56, $b0, $3c, $08
'-' 
'-'                         or      txdata,#$100          'ready byte to transmit
	byte	$00, $c7, $fc, $68
'-'                         shl     txdata,#2
	byte	$02, $c6, $fc, $2c
'-'                         or      txdata,#1
	byte	$01, $c6, $fc, $68
'-'                         mov     txbits,#11
	byte	$0b, $c8, $fc, $a0
'-'                         mov     txcnt,cnt
	byte	$f1, $cb, $bc, $a0
'-' 
'-' :bit                    test    rxtxmode,#%100  wz    'output bit on tx pin according to mode
	byte	$04, $b2, $7c, $62
'-'                         test    rxtxmode,#%010  wc
	byte	$02, $b2, $7c, $61
'-'         if_z_and_c      xor     txdata,#1
	byte	$01, $c6, $e0, $6c
'-'                         shr     txdata,#1       wc
	byte	$01, $c6, $fc, $29
'-'         if_z            muxc    outa,txmask
	byte	$61, $e8, $ab, $70
'-'         if_nz           muxnc   dira,txmask
	byte	$61, $ec, $97, $74
'-'                         add     txcnt,bitticks        'ready next cnt
	byte	$5a, $ca, $bc, $80
'-' 
'-' :wait                   jmpret  txcode,rxcode         'run a chunk of receive code, then return
	byte	$60, $cc, $bc, $5c
'-' 
'-'                         mov     t1,txcnt              'check if bit transmit period done
	byte	$65, $ac, $bc, $a0
'-'                         sub     t1,cnt
	byte	$f1, $ad, $bc, $84
'-'                         cmps    t1,#0           wc
	byte	$00, $ac, $7c, $c1
'-'         if_nc           jmp     #:wait
	byte	$4e, $00, $4c, $5c
'-' 
'-'                         djnz    txbits,#:bit          'another bit to transmit?
	byte	$47, $c8, $fc, $e4
'-' 
'-'                         jmp     #transmit             'byte done, transmit next byte
	byte	$34, $00, $7c, $5c
'-' '
'-' zero    long 0
	byte	$00, $00, $00, $00
'-' '
'-' '
'-' ' Uninitialized data
'-' '
'-' t1                      res     1
'-' t1                      res     1
'-' t2                      res     1
'-' t2                      res     1
'-' t3                      res     1
'-' t3                      res     1
'-' 
'-' rxtxmode                res     1
'-' rxtxmode                res     1
'-' bitticks                res     1
'-' bitticks                res     1
'-' 
'-' rxmask                  res     1
'-' rxmask                  res     1
'-' rxbuff                  res     1
'-' rxbuff                  res     1
'-' rxdata                  res     1
'-' rxdata                  res     1
'-' rxbits                  res     1
'-' rxbits                  res     1
'-' rxcnt                   res     1
'-' rxcnt                   res     1
'-' rxcode                  res     1
'-' rxcode                  res     1
'-' 
'-' txmask                  res     1
'-' txmask                  res     1
'-' txbuff                  res     1
'-' txbuff                  res     1
'-' txdata                  res     1
'-' txdata                  res     1
'-' txbits                  res     1
'-' txbits                  res     1
'-' txcnt                   res     1
'-' txcnt                   res     1
'-' txcode                  res     1
'-' txcode                  res     1
	long
_FN_SPI_Asm_dat_
'-'                 org
'-' '
'-' '' SPI Engine - main loop
'-' '
'-' spi_exe
'-'                 call    #stack_init                      ' make sure our data stack is initialised
	byte	$63, $d3, $fe, $5c
'-' 
'-'                 mov     p1, par                         'get first parameter 0
	byte	$f0, $1b, $bf, $a0
'-'                 rdlong  dumpmem, p1                     'dump routine gets started here
	byte	$8d, $01, $bf, $08
'-'                 add     p1, #4                          'lock Id
	byte	$04, $1a, $ff, $80
'-'                 rdlong  lockid, p1
	byte	$8d, $07, $bf, $08
'-'                 add     p1, #4
	byte	$04, $1a, $ff, $80
'-'                 mov     acmd, p1                         ' we only want the address
	byte	$8d, $03, $bf, $a0
'-'                 add     p1, #4
	byte	$04, $1a, $ff, $80
'-'                 mov     adata, p1                        ' we only want the address
	byte	$8d, $05, $bf, $a0
'-' 
'-'                 mov     W, #CON_CS                      ' Chip Select
	byte	$04, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     cs, W
	byte	$90, $0d, $bf, $a0
'-' 
'-'                 mov     W, #CON_DS                       ' data select pin
	byte	$05, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     cd, W
	byte	$90, $0f, $bf, $a0
'-' 
'-'                 mov     W, #CON_MOSI                     ' Master Out go to Third parameter
	byte	$06, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     mosi, W                      ' data MOSI pin
	byte	$90, $15, $bf, $a0
'-' 
'-'                 mov     W, #CON_MISO                     ' go to Master in parameter
	byte	$07, $20, $ff, $a0
'-'                 call    #PUSHW                        ' this should be serial in
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     miso, W                       ' data miso pin
	byte	$90, $17, $bf, $a0
'-' 
'-'                 mov     W, #CON_CLK                   ' go to Clock parameter
	byte	$08, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     clk, W                       ' data clock pin
	byte	$90, $19, $bf, $a0
'-' 
'-'                 mov     W, #CON_RST                         ' Reset Pin
	byte	$09, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     rst, W                      ' data reset pin
	byte	$90, $11, $bf, $a0
'-' 
'-'                 mov     W, #CON_BLK                    ' Blank screen pin
	byte	$0a, $20, $ff, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #GetPinmask
	byte	$27, $5f, $fe, $5c
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     blk, W                       ' data blank pin
	byte	$90, $13, $bf, $a0
'-' 
'-'                 xor     TRUE, #0    NR,WC               ' clear carry flag
	byte	$00, $d6, $7e, $6d
'-'                 muxnc   outa, blk                       ' blk is hi to enable display
	byte	$89, $e9, $bf, $74
'-'                 muxnc   dira, blk                       ' blk is output put
	byte	$89, $ed, $bf, $74
'-'                 muxnc   outa, cs                        ' cs is low to enable
	byte	$86, $e9, $bf, $74
'-'                 muxnc   dira, cs                        ' cs is output
	byte	$86, $ed, $bf, $74
'-'                 muxnc   outa, rst                       ' rst is low to reset so make it hi for now
	byte	$88, $e9, $bf, $74
'-'                 muxnc   dira, rst                       ' rst is output
	byte	$88, $ed, $bf, $74
'-'                 muxc    outa, cd                        ' dsel is low for command hi for data
	byte	$87, $e9, $bf, $70
'-'                 muxnc   dira, cd                        ' dsel output
	byte	$87, $ed, $bf, $74
'-'                 muxc    outa, mosi                      ' mosi is data
	byte	$8a, $e9, $bf, $70
'-'                 muxnc   dira, mosi                      ' let make it output for now
	byte	$8a, $ed, $bf, $74
'-'                 muxc    outa, clk                       ' clock is low
	byte	$8c, $e9, $bf, $70
'-'                 muxnc   dira, clk                       ' clk output low
	byte	$8c, $ed, $bf, $74
'-'                 muxc    outa, P26
	byte	$78, $e9, $bf, $70
'-'                 muxnc   dira, P26
	byte	$78, $ed, $bf, $74
'-'                 muxnc   outa, P27
	byte	$79, $e9, $bf, $74
'-'                 muxnc   dira, P27
	byte	$79, $ed, $bf, $74
'-'                 lockclr lockid                          ' just incase it is locked
	byte	$07, $06, $7f, $0c
'-' loop
'-'                 call    #P26_toggle
	byte	$cf, $aa, $fd, $5c
'-' '                call    #read_cmd                       ' get the data from hub memory
'-'                 call    #dump_run                       ' this is mainly for debuging just dump the cog memory out to HUB
	byte	$41, $98, $fc, $5c
'-' '                mov     W,cmd                           ' W has the read cmd
'-' '                call    #PUSHW                          ' push w to stack
'-' '                call    #cmd_valid                      ' now test validity of command data
'-' '                call    #POPW                           ' W now has return value command byte
'-' '                cmp     W, #0     wz                    ' W = 0 the command is not valid
'-' '        if_z    jmp     #loop                           '
'-' '                call    #spi_command
'-'                 jmp     #loop
	byte	$3e, $00, $7c, $5c
'-' 
'-' 
'-' 
'-' 
'-' ' Just copy 512 long cog memory out to HUB memory
'-' ' so the dump spin send it out to serial
'-' dump_run
'-'                 mov     t1, #spi_exe
	byte	$00, $e2, $fe, $a0
'-'                 mov     t2, dumpmem
	byte	$80, $e5, $be, $a0
'-'                 mov     t3, dumpsize
	byte	$7f, $e7, $be, $a0
'-' dumpr
'-'                 movs    :dumpmod, t1
	byte	$71, $8d, $bc, $50
'-'                 nop
	byte	$00, $00, $00, $00
'-' :dumpmod        mov     t4, FALSE 't1
	byte	$6c, $e9, $be, $a0
'-'                 nop
	byte	$00, $00, $00, $00
'-'                 wrlong  t4, t2
	byte	$72, $e9, $3e, $08
'-'                 add     t2, #4
	byte	$04, $e4, $fe, $80
'-'                 add     t1, #1
	byte	$01, $e2, $fe, $80
'-'                 djnz    t3, #dumpr
	byte	$44, $e6, $fe, $e4
'-' dump_run_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' ' lets see if command is available
'-' ' uses lock to get the ever change data from the spin memory
'-' read_cmd
'-'                 lockset lockid      wc
	byte	$06, $06, $7f, $0d
'-'         if_c    jmp     #read_cmd_ret                   ' if carry set the lock is set some where else
	byte	$53, $00, $70, $5c
'-'                 nop
	byte	$00, $00, $00, $00
'-' '                rdlong  cmd, acmd                       ' quick look at command
'-' '                rdlong  A, adata                        ' get the command parameter
'-' '                rdlong  cmdata,A
'-' '                add     A,#4
'-' '                rdlong  cmdata1,A
'-' '                add     A,#4
'-' '                rdlong  cmdata2,A
'-' '                add     A,#4
'-' '                rdlong  cmdata3,A
'-'                 wrlong  zero, acmd
	byte	$81, $d5, $3e, $08
'-'                 nop
	byte	$00, $00, $00, $00
'-'                 lockclr lockid
	byte	$07, $06, $7f, $0c
'-' read_cmd_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' write_cmd
'-'                 lockset lockid      wc
	byte	$06, $06, $7f, $0d
'-'         if_c    jmp     #write_cmd_ret
	byte	$59, $00, $70, $5c
'-'                 wrlong  cmd, acmd
	byte	$81, $f5, $3e, $08
'-'                 wrlong  cmdata, adata
	byte	$82, $f7, $3e, $08
'-'                 lockclr lockid
	byte	$07, $06, $7f, $0c
'-' write_cmd_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' Test and get a valid command
'-' ' ( dd -- cmd )
'-' ' return cmd value or false if not valid
'-' cmd_valid
'-'                 call    #dup                            ' make a copy of stack
	byte	$38, $75, $fe, $5c
'-'                 call    #POPW                           'W has the command 32bits
	byte	$53, $b5, $fe, $5c
'-'                 shl     W, #1       wc                  ' move valid bit into C
	byte	$01, $20, $ff, $2d
'-'                 call    #POPW                           ' restore W with command
	byte	$53, $b5, $fe, $5c
'-'         if_c    and     W, #$000000FF                   ' mask off good bits
	byte	$ff, $20, $f3, $60
'-'         if_nc   mov     W, FALSE                        ' if data not valid W is false
	byte	$6c, $21, $8f, $a0
'-'                 call    #PUSHW                          ' W is the command value
	byte	$43, $95, $fe, $5c
'-' cmd_valid_ret   ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' 
'-' spi_command
'-'                 call    #P27_toggle
	byte	$d6, $b2, $fd, $5c
'-' 
'-'                 call    #push_cmd                       ' push the current command on to stack
	byte	$fd, $fe, $fd, $5c
'-'                 call    #b0                             ' get byte 0 of command
	byte	$23, $4d, $fe, $5c
'-'                 call    #cd_clr                         ' 0 for command mode
	byte	$fb, $f8, $fd, $5c
'-'                 call    #cs_clr                         ' 0 for enable
	byte	$f7, $f0, $fd, $5c
'-'                 call    #spi_msb                        ' send out to spi
	byte	$00, $13, $fe, $5c
'-'                 call    #cs_set                         ' disable
	byte	$f5, $ec, $fd, $5c
'-'                 call    #spi_three                      ' three clocks must be a delay
	byte	$0a, $21, $fe, $5c
'-'                 call    #push_cmd                       ' push command
	byte	$fd, $fe, $fd, $5c
'-'                 call    #b2                             ' get the number of data bytes to send
	byte	$19, $3b, $fe, $5c
'-'                 call    #POPW                           '
	byte	$53, $b5, $fe, $5c
'-'                 mov     numbytes, W                     ' save num of byte
	byte	$90, $09, $bf, $a0
'-'                 cmp     numbytes, #0    wz
	byte	$00, $08, $7f, $86
'-'         if_z    jmp     #spi_command_ret                ' if zero no data leave
	byte	$7a, $00, $68, $5c
'-'                 mov     count, #0                       ' start counter at zero
	byte	$00, $0a, $ff, $a0
'-' :spi_loop       mov     W, count
	byte	$85, $21, $bf, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-'                 call    #parameter
	byte	$7b, $9c, $fd, $5c
'-'                 call    #cd_set
	byte	$f9, $f4, $fd, $5c
'-'                 call    #cs_clr
	byte	$f7, $f0, $fd, $5c
'-'                 call    #spi_msb
	byte	$00, $13, $fe, $5c
'-'                 add     count, #1
	byte	$01, $0a, $ff, $80
'-'                 call    #cs_set
	byte	$f5, $ec, $fd, $5c
'-'                 djnz    numbytes, #:spi_loop
	byte	$71, $08, $ff, $e4
'-' spi_command_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' '
'-' ' ( pn -- data )
'-' parameter       call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 cmp     W, #0       wz
	byte	$00, $20, $7f, $86
'-'         if_z    mov     W, cmdata
	byte	$7b, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b3
	byte	$14, $31, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #1       wz
	byte	$01, $20, $7f, $86
'-'         if_z    mov     W, cmdata
	byte	$7b, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b2
	byte	$19, $3b, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #2       wz
	byte	$02, $20, $7f, $86
'-'         if_z    mov     W, cmdata
	byte	$7b, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b1
	byte	$1e, $45, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #3       wz
	byte	$03, $20, $7f, $86
'-'         if_z    mov     W, cmdata
	byte	$7b, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b0
	byte	$23, $4d, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #4       wz
	byte	$04, $20, $7f, $86
'-'         if_z    mov     W, cmdata1
	byte	$7c, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b3
	byte	$14, $31, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #5       wz
	byte	$05, $20, $7f, $86
'-'         if_z    mov     W, cmdata1
	byte	$7c, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b2
	byte	$19, $3b, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #6       wz
	byte	$06, $20, $7f, $86
'-'         if_z    mov     W, cmdata1
	byte	$7c, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b1
	byte	$1e, $45, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #7       wz
	byte	$07, $20, $7f, $86
'-'         if_z    mov     W, cmdata1
	byte	$7c, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b0
	byte	$23, $4d, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #8       wz
	byte	$08, $20, $7f, $86
'-'         if_z    mov     W, cmdata2
	byte	$7d, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b3
	byte	$14, $31, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #9       wz
	byte	$09, $20, $7f, $86
'-'         if_z    mov     W, cmdata2
	byte	$7d, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b2
	byte	$19, $3b, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #10      wz
	byte	$0a, $20, $7f, $86
'-'         if_z    mov     W, cmdata2
	byte	$7d, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b1
	byte	$1e, $45, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #11      wz
	byte	$0b, $20, $7f, $86
'-'         if_z    mov     W, cmdata2
	byte	$7d, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b0
	byte	$23, $4d, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #12      wz
	byte	$0c, $20, $7f, $86
'-'         if_z    mov     W, cmdata3
	byte	$7e, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b3
	byte	$14, $31, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #13      wz
	byte	$0d, $20, $7f, $86
'-'         if_z    mov     W, cmdata3
	byte	$7e, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b2
	byte	$19, $3b, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #14      wz
	byte	$0e, $20, $7f, $86
'-'         if_z    mov     W, cmdata3
	byte	$7e, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b1
	byte	$1e, $45, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 cmp     W, #15      wz
	byte	$0f, $20, $7f, $86
'-'         if_z    mov     W, cmdata3
	byte	$7e, $21, $ab, $a0
'-'         if_z    call    #PUSHW
	byte	$43, $95, $ea, $5c
'-'         if_z    call    #b0
	byte	$23, $4d, $ea, $5c
'-'         if_z    jmp     #parameter_ret
	byte	$ce, $00, $68, $5c
'-'                 mov     W, FALSE
	byte	$6c, $21, $bf, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' parameter_ret   ret
	byte	$00, $00, $7c, $5c
'-' 
'-' P26_toggle
'-'                 call    #NoWaitDelay
	byte	$da, $c6, $fd, $5c
'-'                 cmp     dstate, #0  wz
	byte	$00, $de, $7e, $86
'-'         if_nz   jmp     #P26_toggle_ret
	byte	$d5, $00, $54, $5c
'-'                 test    outa, P26   wz
	byte	$78, $e9, $3f, $62
'-'         if_z    or      outa, P26
	byte	$78, $e9, $ab, $68
'-'         if_nz   andn    outa, P26
	byte	$78, $e9, $97, $64
'-' P26_toggle_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' P27_toggle
'-'                 test    outa, P27   wz
	byte	$79, $e9, $3f, $62
'-'         if_z    or      outa, P27
	byte	$79, $e9, $ab, $68
'-'         if_nz   andn    outa, P27
	byte	$79, $e9, $97, $64
'-' P27_toggle_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' NoWaitDelay
'-'                 cmp     dstate, #DS_START   wz
	byte	$00, $de, $7e, $86
'-'         if_z    call    #NWD_Start
	byte	$e4, $cc, $e9, $5c
'-'         if_z    jmp     #NoWaitDelay_ret
	byte	$e3, $00, $68, $5c
'-'                 cmp     dstate, #DS_TEST    wz
	byte	$01, $de, $7e, $86
'-'         if_z    djnz    cdelay, #NoWaitDelay_ret
	byte	$e3, $e0, $ea, $e4
'-'         if_z    mov     dstate, #DS_END
	byte	$02, $de, $ea, $a0
'-'         if_z    jmp     #NoWaitDelay_ret
	byte	$e3, $00, $68, $5c
'-'                 cmp     dstate, #DS_END wz
	byte	$02, $de, $7e, $86
'-'         if_z    mov     dstate, #DS_START
	byte	$00, $de, $ea, $a0
'-' 'nwd             mov     cdelay,#0
'-' 
'-' NoWaitDelay_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' NWD_Start
'-'                 mov     cdelay, ClockDelay
	byte	$6d, $e1, $be, $a0
'-' '                add     cdelay,cnt
'-'                 mov     dstate, #DS_TEST
	byte	$01, $de, $fe, $a0
'-' NWD_Start_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' mosi_set        or      outa, mosi
	byte	$8a, $e9, $bf, $68
'-' mosi_set_ret    ret
	byte	$00, $00, $7c, $5c
'-' 
'-' mosi_clr        andn    outa, mosi
	byte	$8a, $e9, $bf, $64
'-' mosi_clr_ret    ret
	byte	$00, $00, $7c, $5c
'-' 
'-' clk_set         or      outa, clk
	byte	$8c, $e9, $bf, $68
'-' clk_set_ret     ret
	byte	$00, $00, $7c, $5c
'-' 
'-' clk_clr         andn    outa, clk
	byte	$8c, $e9, $bf, $64
'-' clk_clr_ret     ret
	byte	$00, $00, $7c, $5c
'-' 
'-' clk_toggle
'-'                 test    outa, clk   wz
	byte	$8c, $e9, $3f, $62
'-'         if_z    or      outa, clk
	byte	$8c, $e9, $ab, $68
'-'         if_nz   andn    outa, clk
	byte	$8c, $e9, $97, $64
'-'         if_z    andn    outa, clk
	byte	$8c, $e9, $ab, $64
'-'         if_nz   or      outa, clk
	byte	$8c, $e9, $97, $68
'-' clk_toggle_ret  ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' ' set CS pin high
'-' cs_set          or      outa, cs
	byte	$86, $e9, $bf, $68
'-' cs_set_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' clear CS pin low
'-' cs_clr          andn    outa, cs
	byte	$86, $e9, $bf, $64
'-' cs_clr_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' set CD pin set
'-' cd_set          or      outa, cd
	byte	$87, $e9, $bf, $68
'-' cd_set_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' clear CD pin low
'-' cd_clr          andn    outa, cd
	byte	$87, $e9, $bf, $64
'-' cd_clr_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' ( -- cmd )
'-' push_cmd        mov     W, cmd
	byte	$7a, $21, $bf, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' push_cmd_ret    ret
	byte	$00, $00, $7c, $5c
'-' '################################################################################################################
'-' 'tested OK
'-' ' lets just send data from stack out to spi
'-' ' ( dd -- )
'-' spi_msb                                               ''SHIFTOUT Entry
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 mov     p2, #8
	byte	$08, $1c, $ff, $a0
'-'                 mov     p1, W       wz
	byte	$90, $1b, $bf, $a2
'-'                 shl     p1, #24
	byte	$18, $1a, $ff, $2c
'-' :spi_loop
'-'                 shl     p1, #1      wc
	byte	$01, $1a, $ff, $2d
'-'         if_nc   call    #mosi_clr                      ''          PreSet DataPin LOW
	byte	$e9, $d4, $cd, $5c
'-'         if_c    call    #mosi_set
	byte	$e7, $d0, $f1, $5c
'-' 
'-' 
'-'                 call    #clk_toggle                            ''          PreSet ClockPin LOW
	byte	$ef, $e8, $fd, $5c
'-'                 djnz    p2, #:spi_loop
	byte	$04, $1d, $ff, $e4
'-' spi_msb_ret     ret
	byte	$00, $00, $7c, $5c
'-' '------------------------------------------------------------------------------------------------------------------------------
'-' '
'-' ' documents talk about three clk bits before parameters or data
'-' spi_three       mov     p2, #3
	byte	$03, $1c, $ff, $a0
'-' 
'-' :spi_loop       shl     p1, #1
	byte	$01, $1a, $ff, $2c
'-'         if_nc   call    #mosi_clr
	byte	$e9, $d4, $cd, $5c
'-'         if_c    call    #mosi_set
	byte	$e7, $d0, $f1, $5c
'-'                 call    #clk_toggle
	byte	$ef, $e8, $fd, $5c
'-'                 djnz    p2, #:spi_loop
	byte	$0b, $1d, $ff, $e4
'-' spi_three_ret   ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' 
'-' 'tested OK
'-' ClkDly
'-'                 mov     t6, ClockDelay
	byte	$6d, $ed, $be, $a0
'-' ClkPause        djnz    t6, #ClkPause
	byte	$12, $ed, $fe, $e4
'-' ClkDly_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' 
'-' '----------------------------------------------------------------------------------------------------------------
'-' { keep all the pretend forth here }
'-' 
'-' '----------------------------------------------------------------------------------------------------------------
'-' ' ( dd -- bb )
'-' ' get stack value and return byte 3
'-' ' ( ddxxxxxx -- dd )
'-' b3              call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 shr     W, #24          ' shift 24 - 31 to get 8 bit from top
	byte	$18, $20, $ff, $28
'-'                 and     W, #$FF
	byte	$ff, $20, $ff, $60
'-'                 call    #PUSHW          ' w is now on stack
	byte	$43, $95, $fe, $5c
'-' b3_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' '
'-' ' ( xxddxxxx -- dd )
'-' b2              call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 shr     W, #16          ' shift 16 - 23
	byte	$10, $20, $ff, $28
'-'                 and     W, #$FF         ' make sure we have only 8 bits
	byte	$ff, $20, $ff, $60
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' b2_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' '
'-' ' ( xxxxddxx -- dd )
'-' b1
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 shr     W, #8           ' get 8 - 15
	byte	$08, $20, $ff, $28
'-'                 and     W, #$FF
	byte	$ff, $20, $ff, $60
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' b1_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' '
'-' ' ( xxxxxxdd -- dd )
'-' b0
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 and     W, #$FF
	byte	$ff, $20, $ff, $60
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' b0_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' 
'-' '------------------------------------------------------------------------------------------------------------------------------
'-' ' ( par -- dd )
'-' ' Parameter number spin need will meed to make variable into long
'-' GetPinmask
'-'                 call    #POPW                           ' ( par -- W = par )
	byte	$53, $b5, $fe, $5c
'-'                 shl     W, #2
	byte	$02, $20, $ff, $2c
'-'                 mov     A, par                         '
	byte	$f0, $23, $bf, $a0
'-'                 add     A, W                           ' next we get CS PIN
	byte	$90, $23, $bf, $80
'-'                 rdlong  W, A                           ' read second parameter should be cs_pin number
	byte	$91, $21, $bf, $08
'-'                 mov     A, #1
	byte	$01, $22, $ff, $a0
'-'                 shl     A, W
	byte	$90, $23, $bf, $2c
'-'                 call    #PUSHA                         ' ( -- par dd )
	byte	$4b, $a5, $fe, $5c
'-' GetPinmask_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' '--------------------
'-' ' ( dd -- dd+1 )
'-' inc
'-'                 call    #POPW
	byte	$53, $b5, $fe, $5c
'-'                 add     W, #1
	byte	$01, $20, $ff, $80
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' inc_ret         ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' {
'-' }
'-' 
'-' 
'-' ' read data from top of stack
'-' tw
'-'                 movs    twmod, T                                ' set modify code
	byte	$92, $6d, $be, $50
'-'                 nop
	byte	$00, $00, $00, $00
'-' twmod           mov     W, T                                            ' get top value
	byte	$92, $21, $bf, $a0
'-' tw_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' ' ( n -- n n )
'-' ' duplicate top of stack
'-' dup
'-'                 call    #tw
	byte	$34, $6f, $fe, $5c
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' dup_ret         ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 'Fetch and push to --T and increment A
'-' AR_Inc
'-'                 call    #AR
	byte	$3e, $85, $fe, $5c
'-'                 add     A, #1
	byte	$01, $22, $ff, $80
'-' AR_Inc_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' fetch the contents of Memory address by A into --T
'-' AR
'-'                 movs    ARsmod, A
	byte	$91, $81, $be, $50
'-'                 nop
	byte	$00, $00, $00, $00
'-' ARsmod          mov     W, A
	byte	$91, $21, $bf, $a0
'-'                 call    #PUSHW
	byte	$43, $95, $fe, $5c
'-' AR_ret          ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' push the contents of W into --T
'-' PUSHW           sub     T, #1
	byte	$01, $24, $ff, $84
'-'                 movd    pushwsm, T
	byte	$92, $8d, $be, $54
'-'                 nop
	byte	$00, $00, $00, $00
'-' pushwsm         mov     T, W
	byte	$90, $25, $bf, $a0
'-'                 add     DC, #1
	byte	$01, $2a, $ff, $80
'-'                 mov     S, T
	byte	$92, $27, $bf, $a0
'-'                 sub     S, #1
	byte	$01, $26, $ff, $84
'-' PUSHW_ret       ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' push the contents of A to --T
'-' PUSHA
'-'                 sub     T, #1                                           ' decrement to put data into stack
	byte	$01, $24, $ff, $84
'-'                 movd    pushamod, T                               ' setup self modifying code
	byte	$92, $9d, $be, $54
'-'                 nop                                                             ' pipes need cleaning
	byte	$00, $00, $00, $00
'-' pushamod        mov     T, A                                            ' put W into the top of stack
	byte	$91, $25, $bf, $a0
'-'                 add     DC, #1
	byte	$01, $2a, $ff, $80
'-'                 mov     S, T
	byte	$92, $27, $bf, $a0
'-'                 sub     S, #1
	byte	$01, $26, $ff, $84
'-' PUSHA_ret       ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' data stack pop
'-' ' W has the data from stack
'-' POPW
'-'                 movs    popwsmod, T                               ' self modify code
	byte	$92, $ab, $be, $50
'-'                 nop                                                             ' nop is need for pipes to work
	byte	$00, $00, $00, $00
'-' popwsmod        mov     W, T                                            ' place holder for self modifying code
	byte	$92, $21, $bf, $a0
'-'                 add     T, #1                                           ' increment to the next stack location
	byte	$01, $24, $ff, $80
'-'                 sub     DC, #1
	byte	$01, $2a, $ff, $84
'-'                 mov     S, T
	byte	$92, $27, $bf, $a0
'-'                 sub     S, #1
	byte	$01, $26, $ff, $84
'-' POPW_ret        ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' data stack pop
'-' ' W has the data from stack
'-' POPA
'-'                 movs    popasmod, T                               ' self modify code
	byte	$92, $bb, $be, $50
'-'                 nop                                                             ' nop is need for pipes to work
	byte	$00, $00, $00, $00
'-' popasmod        mov     A, T                                            ' place holder for self modifying code
	byte	$92, $23, $bf, $a0
'-'                 add     T, #1                                           ' increment to the next stack location
	byte	$01, $24, $ff, $80
'-'                 sub     DC, #1
	byte	$01, $2a, $ff, $84
'-'                 mov     S, T
	byte	$92, $27, $bf, $a0
'-'                 sub     S, #1
	byte	$01, $26, $ff, $84
'-' POPA_ret        ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' ' Initialise T to the top of data stack
'-' '
'-' stack_init
'-'                 mov     T, #RS
	byte	$a6, $25, $ff, $a0
'-'                 mov     DC, #0
	byte	$00, $2a, $ff, $a0
'-'                 mov     S, T
	byte	$92, $27, $bf, $a0
'-'                 sub     S, #1                           'secon element on stack
	byte	$01, $26, $ff, $84
'-'                 mov     R, #RS
	byte	$a6, $29, $ff, $a0
'-'                 add     R, #16
	byte	$10, $28, $ff, $80
'-' stack_init_ret
'-'                 ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' {
'-' }
'-' 
'-' 
'-' 
'-' zero            long    0                               ''constants
	byte	$00, $00, $00, $00
'-' 'd0              long    $200
'-' TRUE            long    $FFFFFFFF
	byte	$ff, $ff, $ff, $ff
'-' FALSE           long    $00000000
	byte	$00, $00, $00, $00
'-' 
'-' 
'-' ClockDelay      long    20000
	byte	$20, $4e, $00, $00
'-' ClockState      long    0
	byte	$00, $00, $00, $00
'-' dstate          long    0
	byte	$00, $00, $00, $00
'-' cdelay          long    0
	byte	$00, $00, $00, $00
'-'                                                         ''temp variables
'-' t1              long    0                               ''     Used for DataPin mask     and     COG shutdown
	byte	$00, $00, $00, $00
'-' t2              long    0                               ''     Used for CLockPin mask    and     COG shutdown
	byte	$00, $00, $00, $00
'-' t3              long    0                               ''     Used to hold DataValue SHIFTIN/SHIFTOUT
	byte	$00, $00, $00, $00
'-' t4              long    0                               ''     Used to hold # of Bits
	byte	$00, $00, $00, $00
'-' t5              long    0                               ''     Used for temporary data mask
	byte	$00, $00, $00, $00
'-' t6              long    0                               ''     Used for Clock Delay
	byte	$00, $00, $00, $00
'-' address         long    0                               ''     Used to hold return address of first Argument passed
	byte	$00, $00, $00, $00
'-' 
'-' 'arg0            long    0                               ''arguments passed to/from high-level Spin
'-' 'arg1            long    0
'-' 'arg2            long    0
'-' 'arg3            long    0
'-' 'arg4            long    0
'-' 
'-' '
'-' '                        33222222 22221111 11111100 00000000
'-' '                        10987654 32109876 54321098 76543210
'-' P26             long    %00000100_00000000_00000000_00000000    ' P26
	byte	$00, $00, $00, $04
'-' P27             long    %00001000_00000000_00000000_00000000    ' P27
	byte	$00, $00, $00, $08
'-' 
'-' '                        VVCR
'-' '                        IODW
'-' '                        CONTROL  NUM BITS RESERVED COMMAND
'-' '                        33222222 22221111 11111100 00000000
'-' '                        10987654 32109876 54321098 76543210
'-' cmd             long    %00000000_00000000_00000000_00000000
	byte	$00, $00, $00, $00
'-' 
'-' '                        33222222 22221111 11111100 00000000
'-' '                        10987654 32109876 54321098 76543210
'-' cmdata          long    %00000000_00000000_00000000_00000000
	byte	$00, $00, $00, $00
'-' cmdata1         long    %00000000_00000000_00000000_00000000
	byte	$00, $00, $00, $00
'-' cmdata2         long    %00000000_00000000_00000000_00000000
	byte	$00, $00, $00, $00
'-' cmdata3         long    %00000000_00000000_00000000_00000000
	byte	$00, $00, $00, $00
'-' 
'-' 
'-' ' debug via image dumping the cog memory
'-' dumpsize        long    512
	byte	$00, $02, $00, $00
'-' ' holder for dump memory address
'-' dumpmem         res     1
'-' dumpmem         res     1
'-' 
'-' ' SPI registers
'-' acmd            res     1                               'Addess of command in spin world
'-' acmd            res     1                               'Addess of command in spin world
'-' adata           res     1                               'Address of data in the spin world
'-' adata           res     1                               'Address of data in the spin world
'-' lockid          res     1                               ' lock id
'-' lockid          res     1                               ' lock id
'-' 
'-' numbytes        res     1                               ' holds numberof for parameters
'-' numbytes        res     1                               ' holds numberof for parameters
'-' count           res     1
'-' count           res     1
'-' ' pin config for display
'-' cs              res     1                               ' chip select pin
'-' cs              res     1                               ' chip select pin
'-' cd              res     1                               ' data select pin mainly for displays
'-' cd              res     1                               ' data select pin mainly for displays
'-' rst             res     1                               ' reset pin
'-' rst             res     1                               ' reset pin
'-' blk             res     1                               ' blk pin
'-' blk             res     1                               ' blk pin
'-' mosi            res     1                               ' Master out slave in pin bit location
'-' mosi            res     1                               ' Master out slave in pin bit location
'-' miso            res     1                               ' Master in slave out pin bit location
'-' miso            res     1                               ' Master in slave out pin bit location
'-' clk             res     1
'-' clk             res     1
'-' 
'-' 
'-' 
'-' ' general parameters
'-' p1              res     1                               ' parameter variable
'-' p1              res     1                               ' parameter variable
'-' p2              res     1
'-' p2              res     1
'-' p3              res     1
'-' p3              res     1
'-' 
'-' ' tiny machine forth registers
'-' W               res     1                               ' working register
'-' W               res     1                               ' working register
'-' A               res     1                               ' address register
'-' A               res     1                               ' address register
'-' T               res     1                               ' top of data stack
'-' T               res     1                               ' top of data stack
'-' S               res     1                               ' sub top
'-' S               res     1                               ' sub top
'-' R               res     1                               ' return stack might not use
'-' R               res     1                               ' return stack might not use
'-' DC              res     1                               ' data stack count
'-' DC              res     1                               ' data stack count
'-' DS              res     16                              ' 16 element data stake
'-' DS              res     16                              ' 16 element data stake
'-' RS              res     16                              ' 16 element return stack
'-' RS              res     16                              ' 16 element return stack
'-'                 fit
objmem
	long	0[824]
stackspace
	long	0[1]
	org	COG_BSS_START
_var01
	res	1
_var02
	res	1
_var03
	res	1
_var04
	res	1
_var05
	res	1
_var06
	res	1
arg01
	res	1
arg02
	res	1
arg03
	res	1
arg04
	res	1
arg05
	res	1
arg06
	res	1
arg07
	res	1
local01
	res	1
local02
	res	1
local03
	res	1
local04
	res	1
local05
	res	1
local06
	res	1
local07
	res	1
local08
	res	1
local09
	res	1
local10
	res	1
local11
	res	1
local12
	res	1
local13
	res	1
muldiva_
	res	1
muldivb_
	res	1
LMM_RETREG
	res	1
LMM_FCACHE_START
	res	97
LMM_FCACHE_END
	fit	496
