' driver interface for MSP1281 TFT LCD

CON
    BLK = 5
    CSX = 4
    DCX = 3
    REST = 2
    SCL = 1
    MOSI = 0

    ORIENTATION = 2

    GC9A01A_CAS = $2A           ' Column Address Set
    GC9A01A_PAS = $2B           ' Page Address Set
    GC9A01A_MWRITE = $2C        ' Memory Write
    GC9A01A_MAC = $36           ' Memory Access Control
    GC9A01A_PFS = $3A           ' Pixel Format Set
    PFS_12_BIT = $03
    PFS_16_BIT = $05
    PFS_18_BIT = $06
    GC9A01A_WMC = $3C           ' Write Memory Continue
    GC9A01A_84 = $84            ' unknown
    GC9A01A_85 = $85            ' unknown
    GC9A01A_86 = $86            ' unknown
    GC9A01A_87 = $87            ' unknown
    GC9A01A_88 = $88            ' unknown
    GC9A01A_89 = $89            ' unknown
    GC9A01A_8A = $8A            ' unknown
    GC9A01A_8B = $8B            ' unknown
    GC9A01A_8C = $8C            ' unknown
    GC9A01A_8D = $8D            ' unknown
    GC9A01A_8E = $8E            ' unknown
    GC9A01A_8F = $8F            ' unknown
    GC9A01A_90 = $90            ' unknown

    GC9A01A_DFC = $B6           ' Display Function Control

    GC9A01A_BC = $BC            ' unknown
    GC9A01A_BD = $BD            ' unknown
    GC9A01A_BE = $BE            ' unknown

    GC9A01A_VRC1A = $C3         ' Vreg1a voltage Control
    GC9A01A_VRC1B = $C4         ' Vreg1b voltage Control

    GC9A01A_VRC2A = $C9         ' Vreg2a voltage Control

    GC9A01A_E1 = $E1            ' unknown

    GC9A01A_EB = $EB            ' unknown
    GC9A01A_INREGEN2 = $EF      ' Inter register enable 2
    GC9A01A_INREGEN1 = $FE      ' Inter Register Enable 1
    GC9A01A_FF = $FF            ' unknown

VAR


    long sd[4]


OBJ
    spi : "FN_SPI_Asm"

PUB start : retval | sdata, t


    retval := spi.start(CSX, DCX, MOSI, MOSI, SCL, REST, BLK)

    'spi.cmd_write($aa55aa55, 0)

    ifnot(retval == 0)

        spi.que_command_wait(GC9A01A_INREGEN2)

        spi.que_command_wait(GC9A01A_EB)
        spi.que_data_wait($14)

        spi.que_command_wait(GC9A01A_INREGEN1)

        spi.que_command_wait(GC9A01A_INREGEN2)

        spi.que_command_wait(GC9A01A_EB)
        spi.que_data_wait($14)

        spi.que_command_wait(GC9A01A_84)
        spi.que_data_wait($40)

        spi.que_command_wait(GC9A01A_85)
        spi.que_data_wait($FF)

        spi.que_command_wait(GC9A01A_86)
        spi.que_data_wait($FF)

        spi.que_command_wait(GC9A01A_87)
        spi.que_data_wait($FF)

        spi.que_command_wait(GC9A01A_88)
        spi.que_data_wait($0A)

        spi.que_command_wait(GC9A01A_89)
        spi.que_data_wait($21)

        spi.que_command_wait(GC9A01A_8A)
        spi.que_data_wait($00)

        spi.que_command_wait(GC9A01A_8B)
        spi.que_data_wait($80)

        spi.que_command_wait(GC9A01A_8C)
        spi.que_data_wait($01)

        spi.que_command_wait(GC9A01A_8D)
        spi.que_data_wait($01)

        spi.que_command_wait(GC9A01A_8E)
        spi.que_data_wait($FF)

        spi.que_command_wait(GC9A01A_8F)
        spi.que_data_wait($FF)

        spi.que_command_wait(GC9A01A_DFC)
        spi.que_data_wait(0)
        spi.que_data_wait(0x20)

        spi.que_command_wait(GC9A01A_MAC)

        case ORIENTATION
            0 : spi.que_data_wait($18)
            1 : spi.que_data_wait($28)
            2 : spi.que_data_wait($48)
            OTHER : spi.que_data_wait($88)

        spi.que_command_wait(GC9A01A_PFS)
        spi.que_data_wait(PFS_18_BIT)

        spi.que_command_wait(GC9A01A_90)
        spi.que_data_wait(8)
        spi.que_data_wait(8)
        spi.que_data_wait(8)
        spi.que_data_wait(8)

        spi.que_command_wait(GC9A01A_BD)
        spi.que_data_wait(6)

        spi.que_command_wait(GC9A01A_BC)
        spi.que_data_wait(0)

        spi.que_command_wait(GC9A01A_FF)
        spi.que_data_wait($60)
        spi.que_data_wait($01)
        spi.que_data_wait($04)

        spi.que_command_wait(GC9A01A_VRC1A)
        spi.que_data_wait($13)

        spi.que_command_wait(GC9A01A_VRC1B)
        spi.que_data_wait($13)

        spi.que_command_wait(GC9A01A_VRC2A)
        spi.que_data_wait($22)

        spi.que_command_wait(GC9A01A_BE)
        spi.que_data_wait($11)

        spi.que_command_wait(GC9A01A_E1)
        spi.que_data_wait($10)
        spi.que_data_wait($0E)

        spi.que_command_wait($DF)
        spi.que_data_wait($21)
        spi.que_data_wait($0c)
        spi.que_data_wait($02)

        spi.que_command_wait($F0)
        spi.que_data_wait($45)
        spi.que_data_wait($09)
        spi.que_data_wait($08)
        spi.que_data_wait($08)
        spi.que_data_wait($26)
        spi.que_data_wait($2A)

        spi.que_command_wait($F1)
        spi.que_data_wait($43)
        spi.que_data_wait($70)
        spi.que_data_wait($72)
        spi.que_data_wait($36)
        spi.que_data_wait($37)
        spi.que_data_wait($6F)

        spi.que_command_wait($F2)
        spi.que_data_wait($45)
        spi.que_data_wait($09)
        spi.que_data_wait($08)
        spi.que_data_wait($08)
        spi.que_data_wait($26)
        spi.que_data_wait($2A)

        spi.que_command_wait($F3)
        spi.que_data_wait($43)
        spi.que_data_wait($70)
        spi.que_data_wait($72)
        spi.que_data_wait($36)
        spi.que_data_wait($37)
        spi.que_data_wait($6F)

        spi.que_command_wait($ED)
        spi.que_data_wait($1B)
        spi.que_data_wait($0B)

        spi.que_command_wait($AE)
        spi.que_data_wait($77)

        spi.que_command_wait($CD)
        spi.que_data_wait($63)

        spi.que_command_wait($70)
        spi.que_data_wait($07)
        spi.que_data_wait($07)
        spi.que_data_wait($04)
        spi.que_data_wait($0E)
        spi.que_data_wait($0F)
        spi.que_data_wait($09)
        spi.que_data_wait($07)
        spi.que_data_wait($08)
        spi.que_data_wait($03)

        spi.que_command_wait($E8)
        spi.que_data_wait($34)

        spi.que_command_wait($62)
        spi.que_data_wait($18)
        spi.que_data_wait($0D)
        spi.que_data_wait($71)
        spi.que_data_wait($ED)
        spi.que_data_wait($70)
        spi.que_data_wait($70)
        spi.que_data_wait($18)
        spi.que_data_wait($0F)
        spi.que_data_wait($71)
        spi.que_data_wait($EF)
        spi.que_data_wait($70)
        spi.que_data_wait($70)

        spi.que_command_wait($63)
        spi.que_data_wait($18)
        spi.que_data_wait($11)
        spi.que_data_wait($71)
        spi.que_data_wait($F1)
        spi.que_data_wait($70)
        spi.que_data_wait($70)
        spi.que_data_wait($18)
        spi.que_data_wait($13)
        spi.que_data_wait($71)
        spi.que_data_wait($F3)
        spi.que_data_wait($70)
        spi.que_data_wait($70)

        spi.que_command_wait($64)
        spi.que_data_wait($28)
        spi.que_data_wait($29)
        spi.que_data_wait($F1)
        spi.que_data_wait($01)
        spi.que_data_wait($F1)
        spi.que_data_wait($00)
        spi.que_data_wait($07)

        spi.que_command_wait($66)
        spi.que_data_wait($3C)
        spi.que_data_wait($00)
        spi.que_data_wait($CD)
        spi.que_data_wait($67)
        spi.que_data_wait($45)
        spi.que_data_wait($45)
        spi.que_data_wait($10)
        spi.que_data_wait($00)
        spi.que_data_wait($00)
        spi.que_data_wait($00)

        spi.que_command_wait($67)
        spi.que_data_wait($00)
        spi.que_data_wait($3C)
        spi.que_data_wait($00)
        spi.que_data_wait($00)
        spi.que_data_wait($00)
        spi.que_data_wait($01)
        spi.que_data_wait($54)
        spi.que_data_wait($10)
        spi.que_data_wait($32)
        spi.que_data_wait($98)

        spi.que_command_wait($74)
        spi.que_data_wait($10)
        spi.que_data_wait($85)
        spi.que_data_wait($80)
        spi.que_data_wait($00)
        spi.que_data_wait($00)
        spi.que_data_wait($4E)
        spi.que_data_wait($00)

        spi.que_command_wait($98)
        spi.que_data_wait($3e)
        spi.que_data_wait($07)

        spi.que_command_wait($35)
        spi.que_command_wait($21)

        spi.que_command_wait($11)
        waitcnt(clkfreq / 12000 + cnt) 'GC9A01_delay(120);
        spi.que_command_wait($29)
        waitcnt(clkfreq / 20000 + cnt) 'GC9A01_delay(20);
 '       bl(1)

    return

PUB get_spi : retval
    retval := spi.get_asm

PUB set_frame(xs,ys,xe,ye)
    spi.que_command_wait(GC9A01A_CAS)
    spi.que_data_wait(xs[1])
    spi.que_data_wait(xs[0])
    spi.que_data_wait(xe[1])
    spi.que_data_wait(xe[0])
    'GC9A01_write_data(data, sizeof(data));

    spi.que_command_wait(GC9A01A_PAS)
    spi.que_data_wait(ys[1])
    spi.que_data_wait(ys[0])
    spi.que_data_wait(ye[1])
    spi.que_data_wait(ye[0])
    'GC9A01_write_data(data, sizeof(data));

